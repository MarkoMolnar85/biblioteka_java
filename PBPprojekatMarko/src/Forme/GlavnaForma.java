package Forme;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import Entiteti.Citaoc;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GlavnaForma {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GlavnaForma window = new GlavnaForma();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GlavnaForma() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Upis", null, panel, null);
		panel.setLayout(null);
		
		JButton btnCitaoc = new JButton("Upis citaoca izdavanje i vracanje knjige");
		btnCitaoc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CitaociForma citaociFrm = new CitaociForma();
				citaociFrm.setVisible(true);
			}
		});
		btnCitaoc.setBounds(152, 56, 253, 32);
		panel.add(btnCitaoc);
		
		JButton bnAutori = new JButton("Upis novih autora");
		bnAutori.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AutoriForma frm = new AutoriForma();
				frm.setVisible(true);
			}
		});
		bnAutori.setBounds(152, 88, 253, 32);
		panel.add(bnAutori);
		
		JButton btnCasopisi = new JButton("Upis novih casopisa");
		btnCasopisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CasopisiForma frm = new CasopisiForma();
				frm.setVisible(true);
			}
		});
		btnCasopisi.setBounds(152, 120, 253, 32);
		panel.add(btnCasopisi);
		
		JButton btnIzdavaci = new JButton("Upis novih izdavaca");
		btnIzdavaci.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IzdavacForma frm = new IzdavacForma();
				frm.setVisible(true);
			}
		});
		btnIzdavaci.setBounds(152, 151, 253, 32);
		panel.add(btnIzdavaci);
		
		JButton btnKnjige = new JButton("Upis novih knjiga");
		btnKnjige.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KnjigeForma frm = new KnjigeForma();
				frm.setVisible(true);
			}
		});
		btnKnjige.setBounds(152, 183, 253, 32);
		panel.add(btnKnjige);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Pregled", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnUpitCitaoci = new JButton("Prikazi citaoce");
		btnUpitCitaoci.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UpitiForma frm = new UpitiForma("citaoci");
				frm.setVisible(true);
			}
		});
		btnUpitCitaoci.setBounds(186, 21, 180, 37);
		panel_1.add(btnUpitCitaoci);
		
		JButton btnUpitTelefoni = new JButton("Prikazi telefone");
		btnUpitTelefoni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpitiForma frm = new UpitiForma("telefoni");
				frm.setVisible(true);
			}
		});
		btnUpitTelefoni.setBounds(186, 59, 180, 37);
		panel_1.add(btnUpitTelefoni);
		
		JButton btnIzdateKnjigeUpit = new JButton("Prikazi izdate knjige");
		btnIzdateKnjigeUpit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpitiForma frm = new UpitiForma("izdate knjige");
				frm.setVisible(true);
			}
		});
		btnIzdateKnjigeUpit.setBounds(186, 96, 180, 37);
		panel_1.add(btnIzdateKnjigeUpit);
		
		JButton btnKnjigeUpit = new JButton("Prikazi knjige na stanju");
		btnKnjigeUpit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpitiForma frm = new UpitiForma("knjige");
				frm.setVisible(true);
			}
		});
		btnKnjigeUpit.setBounds(186, 133, 180, 37);
		panel_1.add(btnKnjigeUpit);
		
		JButton btnIzdanjaAutoriUpit = new JButton("Prikazi izdanja i autore");
		btnIzdanjaAutoriUpit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpitiForma frm = new UpitiForma("Izdanja i autori");
				frm.setVisible(true);
			}
		});
		btnIzdanjaAutoriUpit.setBounds(186, 170, 180, 37);
		panel_1.add(btnIzdanjaAutoriUpit);
		
		JButton btnCasopisiUpit = new JButton("Prikazi casopise i njihove radove");
		btnCasopisiUpit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpitiForma frm = new UpitiForma("Casopisi radovi");
				frm.setVisible(true);
			}
		});
		btnCasopisiUpit.setBounds(186, 207, 180, 37);
		panel_1.add(btnCasopisiUpit);
		
		JButton btnKnjigeZanroviUpit = new JButton("Prikazi broj knjiga za zanrove");
		btnKnjigeZanroviUpit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpitiForma frm = new UpitiForma("Broj knjiga po zanru");
				frm.setVisible(true);
			}
		});
		btnKnjigeZanroviUpit.setBounds(186, 246, 180, 37);
		panel_1.add(btnKnjigeZanroviUpit);
		
		JPanel panelPregledParametar = new JPanel();
		tabbedPane.addTab("Pregled sa ulaznim parametrom", null, panelPregledParametar, null);
		panelPregledParametar.setLayout(null);
		
		JButton btnPronadjiCitaocePara = new JButton("Pronadji citaoce");
		btnPronadjiCitaocePara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PitalicaForma frm = new PitalicaForma("citaoci");
				frm.setVisible(true);
			}
		});
		btnPronadjiCitaocePara.setBounds(183, 69, 191, 42);
		panelPregledParametar.add(btnPronadjiCitaocePara);
		
		JButton btnPronadjiKnjigePara = new JButton("Pronadji knjige");
		btnPronadjiKnjigePara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PitalicaForma frm = new PitalicaForma("knjige");
				frm.setVisible(true);
			}
		});
		btnPronadjiKnjigePara.setBounds(183, 111, 191, 42);
		panelPregledParametar.add(btnPronadjiKnjigePara);
		
		JButton btnPronadjiRadovePara = new JButton("Pronadji radove");
		btnPronadjiRadovePara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PitalicaForma frm = new PitalicaForma("radovi");
				frm.setVisible(true);
			}
		});
		btnPronadjiRadovePara.setBounds(183, 153, 191, 42);
		panelPregledParametar.add(btnPronadjiRadovePara);
	}
}
