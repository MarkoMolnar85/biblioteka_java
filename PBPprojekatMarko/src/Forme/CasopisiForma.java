package Forme;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DAO.KonekcijaZaCasopis;
import Kontroleri.KontrolerZaCasopis;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CasopisiForma extends JFrame {

	private JPanel contentPane;
	private JTextField txtIssn;
	private JTextField txtNaslov;
	private JTextField txtKataloskiBroj;
	private JTextField txtUrednik;
	private JTextField txtRadNaslov;
	private JTextField txtRadIssn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CasopisiForma frame = new CasopisiForma();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CasopisiForma() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 482, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelCasopis = new JPanel();
		tabbedPane.addTab("Casopis", null, panelCasopis, null);
		panelCasopis.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ISSN");
		lblNewLabel.setBounds(21, 28, 122, 14);
		panelCasopis.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Naslov");
		lblNewLabel_1.setBounds(21, 69, 122, 14);
		panelCasopis.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Kataloski broj");
		lblNewLabel_2.setBounds(21, 116, 122, 14);
		panelCasopis.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Urednik");
		lblNewLabel_3.setBounds(21, 160, 122, 14);
		panelCasopis.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Izdavac");
		lblNewLabel_4.setBounds(21, 200, 122, 14);
		panelCasopis.add(lblNewLabel_4);
		
		JLabel lblPoruka = new JLabel("Poruka");
		lblPoruka.setBounds(10, 264, 236, 14);
		panelCasopis.add(lblPoruka);
		
		txtIssn = new JTextField();
		txtIssn.setBounds(153, 25, 236, 20);
		panelCasopis.add(txtIssn);
		txtIssn.setColumns(10);
		
		txtNaslov = new JTextField();
		txtNaslov.setBounds(153, 66, 236, 20);
		panelCasopis.add(txtNaslov);
		txtNaslov.setColumns(10);
		
		txtKataloskiBroj = new JTextField();
		txtKataloskiBroj.setBounds(153, 110, 236, 20);
		panelCasopis.add(txtKataloskiBroj);
		txtKataloskiBroj.setColumns(10);
		
		txtUrednik = new JTextField();
		txtUrednik.setBounds(153, 157, 236, 20);
		panelCasopis.add(txtUrednik);
		txtUrednik.setColumns(10);
		
		JComboBox cmbIzdavac = new JComboBox();
		cmbIzdavac.setBounds(153, 197, 236, 20);
		panelCasopis.add(cmbIzdavac);
		
		JButton btnUnos = new JButton("UNOS");
		btnUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				KontrolerZaCasopis kontrola = new KontrolerZaCasopis();
				String poruka = kontrola.ProveraCasopisa(txtIssn.getText(), txtNaslov.getText(), txtKataloskiBroj.getText(),
						txtUrednik.getText(), cmbIzdavac.getSelectedItem().toString());
				lblPoruka.setText(poruka);
				if(txtRadIssn !=null && txtRadIssn.getText().length() > 0)
				txtRadIssn.setText(txtIssn.getText());
			}
		});
		btnUnos.setBounds(268, 250, 121, 33);
		panelCasopis.add(btnUnos);
		
		JPanel panerRad = new JPanel();
		tabbedPane.addTab("Rad", null, panerRad, null);
		panerRad.setLayout(null);
		
		JLabel lblNewLabel_5 = new JLabel("Naslov");
		lblNewLabel_5.setBounds(21, 31, 142, 14);
		panerRad.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("ISSN");
		lblNewLabel_6.setBounds(21, 73, 142, 14);
		panerRad.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Autor");
		lblNewLabel_7.setBounds(21, 118, 142, 14);
		panerRad.add(lblNewLabel_7);
		
		txtRadNaslov = new JTextField();
		txtRadNaslov.setBounds(173, 28, 188, 20);
		panerRad.add(txtRadNaslov);
		txtRadNaslov.setColumns(10);
		
		txtRadIssn = new JTextField();
		txtRadIssn.setBounds(173, 70, 188, 20);
		panerRad.add(txtRadIssn);
		txtRadIssn.setColumns(10);
		
		JComboBox cmbRadAutori = new JComboBox();
		cmbRadAutori.setBounds(173, 115, 188, 20);
		panerRad.add(cmbRadAutori);
		
		JLabel lblRadPoruka = new JLabel("Poruka");
		lblRadPoruka.setBounds(21, 264, 283, 14);
		panerRad.add(lblRadPoruka);
		
		JButton btnRadUnos = new JButton("UNOS");
		btnRadUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolerZaCasopis kontrola = new KontrolerZaCasopis();
				String poruka = kontrola.ProveraRada(txtRadNaslov.getText(), txtRadIssn.getText(), cmbRadAutori.getSelectedItem().toString());
				lblRadPoruka.setText(poruka);
			}
		});
		btnRadUnos.setBounds(330, 243, 111, 35);
		panerRad.add(btnRadUnos);
		
		
		KonekcijaZaCasopis konekcija = new KonekcijaZaCasopis();
		ArrayList<String>listaZaBogacenjeIzdavaca = konekcija.BogacenjeKomboBoxaIzdavac();
		for(int i = 0; i < listaZaBogacenjeIzdavaca.size(); i++)
		{
			cmbIzdavac.addItem(listaZaBogacenjeIzdavaca.get(i));
		}
		ArrayList<String>listaZaBogacenjeAutora = konekcija.BogacenjeKomboBoxaAutoraUradovima();
		for(int i = 0; i < listaZaBogacenjeAutora.size(); i++)
		{
			cmbRadAutori.addItem(listaZaBogacenjeAutora.get(i));
		}
	}
}





















