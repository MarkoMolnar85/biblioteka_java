package Forme;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DAO.KonekcijaZaKnjige;
import Entiteti.Knjiga;
import Kontroleri.Kontroler;
import Kontroleri.KontrolorKnjige;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class KnjigeForma extends JFrame {

	private JPanel contentPane;
	private JTextField txtZanNaziv;
	private JTextField txtKnjigaNazivZanra;
	private JTextField txtKnjigaKataloskiBroj;
	private JTextField txtKnjigaNaziv;
	private JTextField txtKnjigaMesto;
	int brojac = 0;
	private JTextField txtIzdanjaKataloskiBroj;
	private JTextField txtIzdanjaDatum;
	private JTextField txtIzdanjaISBN;
	private JTextField txtIzdanjaBrojStrana;
	private JTextField txtIzdanjaCena;
	private JTextField txtIzdanjaBrojIzdanja;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KnjigeForma frame = new KnjigeForma();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public KnjigeForma() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Zanr", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNoviZanr = new JLabel("Novi zanr");
		lblNoviZanr.setBounds(22, 29, 95, 14);
		panel.add(lblNoviZanr);
		
		JLabel lblIzborZanra = new JLabel("Izbor zanra");
		lblIzborZanra.setBounds(22, 67, 95, 14);
		panel.add(lblIzborZanra);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Izdanja", null, panel_2, null);
		panel_2.setLayout(null);
		
		txtZanNaziv = new JTextField();
		txtZanNaziv.setBounds(127, 26, 181, 20);
		panel.add(txtZanNaziv);
		txtZanNaziv.setColumns(10);
		
		JComboBox cmbZanrNaziv = new JComboBox();
		cmbZanrNaziv.setBounds(127, 64, 181, 20);
		panel.add(cmbZanrNaziv);
		
		//obogacivanje kombo boxa zanra
		KonekcijaZaKnjige konekcija = new KonekcijaZaKnjige();
		ArrayList<String>listaZaKomboBoxZanrovi = konekcija.ObogoacivanjeKomboBoxaZanrovi();
		for(int i = 0; i < listaZaKomboBoxZanrovi.size(); i++)
		{
			cmbZanrNaziv.addItem(listaZaKomboBoxZanrovi.get(i));
		}
		
		
		JPanel panel_knjiga = new JPanel();
		tabbedPane.addTab("Knjige", null, panel_knjiga, null);
		panel_knjiga.setLayout(null);
		
		JLabel lblNazivZanra = new JLabel("Naziv zanra");
		lblNazivZanra.setBounds(10, 24, 143, 14);
		panel_knjiga.add(lblNazivZanra);
		
		JLabel lblNewLabel = new JLabel("Katalosko broj");
		lblNewLabel.setBounds(10, 58, 143, 14);
		panel_knjiga.add(lblNewLabel);
		
		JLabel lblIzborKataloskogBroja = new JLabel("Izbor kataloskog broja");
		lblIzborKataloskogBroja.setBounds(10, 93, 143, 14);
		panel_knjiga.add(lblIzborKataloskogBroja);
		
		JLabel lblNazivKnjige = new JLabel("Naziv knjige");
		lblNazivKnjige.setBounds(10, 129, 143, 14);
		panel_knjiga.add(lblNazivKnjige);
		
		JLabel lblNewLabel_1 = new JLabel("Ostecenje");
		lblNewLabel_1.setBounds(10, 163, 143, 14);
		panel_knjiga.add(lblNewLabel_1);
		
		JLabel lblMesto = new JLabel("Mesto");
		lblMesto.setBounds(10, 197, 143, 14);
		panel_knjiga.add(lblMesto);
		
		JLabel lblIzdanjaPoruka = new JLabel("Poruka");
		lblIzdanjaPoruka.setBounds(10, 349, 254, 14);
		panel_2.add(lblIzdanjaPoruka);
		
		
		txtKnjigaNazivZanra = new JTextField();
		txtKnjigaNazivZanra.setBounds(163, 21, 164, 20);
		panel_knjiga.add(txtKnjigaNazivZanra);
		txtKnjigaNazivZanra.setColumns(10);
		
		txtKnjigaKataloskiBroj = new JTextField();
		txtKnjigaKataloskiBroj.setBounds(163, 55, 164, 20);
		panel_knjiga.add(txtKnjigaKataloskiBroj);
		txtKnjigaKataloskiBroj.setColumns(10);
		
		JComboBox cmbKnjigaIzborKataloskog = new JComboBox();
		cmbKnjigaIzborKataloskog.setBounds(163, 90, 164, 20);
		panel_knjiga.add(cmbKnjigaIzborKataloskog);
		
		txtKnjigaNaziv = new JTextField();
		txtKnjigaNaziv.setBounds(163, 126, 164, 20);
		panel_knjiga.add(txtKnjigaNaziv);
		txtKnjigaNaziv.setColumns(10);
		
		txtKnjigaMesto = new JTextField();
		txtKnjigaMesto.setBounds(163, 194, 164, 20);
		panel_knjiga.add(txtKnjigaMesto);
		txtKnjigaMesto.setColumns(10);
		
		JComboBox cmbKnjigaOstecenje = new JComboBox();
		cmbKnjigaOstecenje.setModel(new DefaultComboBoxModel(new String[] {"nema", "srednje", "jako"}));
		cmbKnjigaOstecenje.setBounds(163, 160, 164, 20);
		panel_knjiga.add(cmbKnjigaOstecenje);
		
		JLabel lblKnjigaPoruka = new JLabel("Poruka");
		lblKnjigaPoruka.setBounds(10, 305, 258, 14);
		panel_knjiga.add(lblKnjigaPoruka);
		
		txtIzdanjaKataloskiBroj = new JTextField();
		txtIzdanjaKataloskiBroj.setBounds(117, 29, 147, 20);
		panel_2.add(txtIzdanjaKataloskiBroj);
		txtIzdanjaKataloskiBroj.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Kataloski broj");
		lblNewLabel_2.setBounds(10, 32, 97, 14);
		panel_2.add(lblNewLabel_2);
		
		JLabel lblDatum = new JLabel("Datum");
		lblDatum.setBounds(10, 70, 97, 14);
		panel_2.add(lblDatum);
		
		txtIzdanjaDatum = new JTextField();
		txtIzdanjaDatum.setBounds(117, 67, 147, 20);
		panel_2.add(txtIzdanjaDatum);
		txtIzdanjaDatum.setColumns(10);
		
		JLabel lblIsbn = new JLabel("Isbn");
		lblIsbn.setBounds(10, 113, 97, 14);
		panel_2.add(lblIsbn);
		
		txtIzdanjaISBN = new JTextField();
		txtIzdanjaISBN.setBounds(117, 110, 147, 20);
		panel_2.add(txtIzdanjaISBN);
		txtIzdanjaISBN.setColumns(10);
		
		txtIzdanjaBrojStrana = new JTextField();
		txtIzdanjaBrojStrana.setBounds(117, 146, 147, 20);
		panel_2.add(txtIzdanjaBrojStrana);
		txtIzdanjaBrojStrana.setColumns(10);
		
		JLabel lblBrojStrana = new JLabel("Broj strana");
		lblBrojStrana.setBounds(10, 149, 97, 14);
		panel_2.add(lblBrojStrana);
		
		JLabel lblNewLabel_3 = new JLabel("Cena");
		lblNewLabel_3.setBounds(10, 186, 97, 14);
		panel_2.add(lblNewLabel_3);
		
		txtIzdanjaCena = new JTextField();
		txtIzdanjaCena.setBounds(117, 183, 147, 20);
		panel_2.add(txtIzdanjaCena);
		txtIzdanjaCena.setColumns(10);
		
		JLabel lblBrojizdanja = new JLabel("BrojIzdanja");
		lblBrojizdanja.setBounds(10, 226, 97, 14);
		panel_2.add(lblBrojizdanja);
		
		txtIzdanjaBrojIzdanja = new JTextField();
		txtIzdanjaBrojIzdanja.setBounds(117, 223, 147, 20);
		panel_2.add(txtIzdanjaBrojIzdanja);
		txtIzdanjaBrojIzdanja.setColumns(10);
		
		JLabel lblIzborIzdavaca = new JLabel("Izbor izdavaca");
		lblIzborIzdavaca.setBounds(10, 266, 97, 14);
		panel_2.add(lblIzborIzdavaca);
		
		JComboBox cmbIzdanjaIzborIzdavaca = new JComboBox();
		cmbIzdanjaIzborIzdavaca.setBounds(117, 263, 147, 20);
		panel_2.add(cmbIzdanjaIzborIzdavaca);
		
		JComboBox cmbIzdanjaVez = new JComboBox();
		cmbIzdanjaVez.setModel(new DefaultComboBoxModel(new String[] {"tvrdi", "meki"}));
		cmbIzdanjaVez.setBounds(117, 298, 147, 20);
		panel_2.add(cmbIzdanjaVez);
		
		JLabel lblNewLabel_4 = new JLabel("Vez");
		lblNewLabel_4.setBounds(10, 301, 46, 14);
		panel_2.add(lblNewLabel_4);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Autori", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel_5 = new JLabel("Izbor statusa");
		lblNewLabel_5.setBounds(10, 27, 106, 14);
		panel_3.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Isbn");
		lblNewLabel_6.setBounds(10, 74, 106, 14);
		panel_3.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Izbor autora");
		lblNewLabel_7.setBounds(10, 122, 106, 14);
		panel_3.add(lblNewLabel_7);
		
		JComboBox cmbAutoriStatus = new JComboBox();
		cmbAutoriStatus.setModel(new DefaultComboBoxModel(new String[] {"autor", "koautor"}));
		cmbAutoriStatus.setBounds(126, 24, 199, 20);
		panel_3.add(cmbAutoriStatus);
		
		JComboBox cmbAutoriIsbn = new JComboBox();
		cmbAutoriIsbn.setBounds(126, 71, 199, 20);
		panel_3.add(cmbAutoriIsbn);
		
		JComboBox cmbAutoriIzborAutora = new JComboBox();
		cmbAutoriIzborAutora.setBounds(126, 119, 199, 20);
		panel_3.add(cmbAutoriIzborAutora);
		
		JLabel lblAutoriPoruka = new JLabel("Poruka");
		lblAutoriPoruka.setBounds(10, 349, 289, 14);
		panel_3.add(lblAutoriPoruka);
		
		JButton btnAutoriUpis = new JButton("UPIS");
		btnAutoriUpis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolorKnjige kontrola = new KontrolorKnjige();
				String poruka = kontrola.ProveraUnosaAutoraZaKnjige(cmbAutoriStatus.getSelectedItem().toString(),
						cmbAutoriIsbn.getSelectedItem().toString(), cmbAutoriIzborAutora.getSelectedItem().toString());
				lblAutoriPoruka.setText(poruka);
			}
		});
		btnAutoriUpis.setBounds(328, 327, 112, 41);
		panel_3.add(btnAutoriUpis);
		
		JLabel lblZanrPoruka = new JLabel("Poruka");
		lblZanrPoruka.setBounds(22, 316, 258, 14);
		panel.add(lblZanrPoruka);
		
		
		JButton btnZanrUnos = new JButton("UPISI");
		btnZanrUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolorKnjige kontrolor = new KontrolorKnjige();
				String poruka = kontrolor.ProveraZanra(txtZanNaziv.getText());
				lblZanrPoruka.setText(poruka);
				cmbZanrNaziv.insertItemAt(txtZanNaziv.getText(), 0);
				
			}
		});
		btnZanrUnos.setBounds(290, 309, 154, 29);
		panel.add(btnZanrUnos);
		
		JButton btnKnjigaUnos = new JButton("UPISI");
		btnKnjigaUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolorKnjige kon = new KontrolorKnjige();
				String poruka = kon.ProveraUpisaKnjige(txtKnjigaNazivZanra.getText(), txtKnjigaKataloskiBroj.getText(), 
						txtKnjigaNaziv.getText(), cmbKnjigaOstecenje.getSelectedItem().toString(), txtKnjigaMesto.getText());
				lblKnjigaPoruka.setText(poruka);
				if(poruka.equals("Nova knjiga je upisana"));
				cmbKnjigaIzborKataloskog.insertItemAt(txtKnjigaKataloskiBroj.getText() + "-" + txtKnjigaNaziv.getText(), 1);
				
				if(txtKnjigaKataloskiBroj != null)
				{
					txtIzdanjaKataloskiBroj.setText(txtKnjigaKataloskiBroj.getText());
				}
			}
		});
		btnKnjigaUnos.setBounds(284, 295, 127, 35);
		panel_knjiga.add(btnKnjigaUnos);
		

		/*JButton btnPrikazi = new JButton("Prikazi");
		btnPrikazi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KonekcijaZaKnjige konekc = new KonekcijaZaKnjige();
				String cela = cmbKnjigaIzborKataloskog.getSelectedItem().toString();
				String nova = "";
				for(int i =0; i < cela.length(); i++)
				{
					char katakter = cela.charAt(i);
					if(katakter == '-')break;
					nova += katakter;
					
				}
				int n = Integer.parseInt(nova);
				Knjiga knjiga = konekc.SelectKnjigeNaOsnovuNazivaZanra(n);
				txtKnjigaKataloskiBroj.setText(""+ knjiga.getKataloskiBroj());
				txtKnjigaNaziv.setText(knjiga.getNaziv());
				txtKnjigaMesto.setText(knjiga.getMesto());
			}
		});
		btnPrikazi.setBounds(284, 254, 127, 30);
		panel_knjiga.add(btnPrikazi);*/
		
		cmbZanrNaziv.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				txtZanNaziv.setText(cmbZanrNaziv.getSelectedItem().toString());
				txtKnjigaNazivZanra.setText(cmbZanrNaziv.getSelectedItem().toString());
			}
		});
		
			
		
		cmbZanrNaziv.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg) {
				// TODO Auto-generated method stub
				KonekcijaZaKnjige konekcija2 = new KonekcijaZaKnjige();
				String komboZanr = cmbZanrNaziv.getSelectedItem().toString();
				ArrayList<String>listaZaKomboBoxKataloskiKnjige = konekcija2.ObogacivanjeKomboBoxaKataloskiBrojeviKnjiga(komboZanr);
				if(arg.getStateChange() ==1)
				{
					cmbKnjigaIzborKataloskog.removeAllItems();
					for(int i = 0; i < listaZaKomboBoxKataloskiKnjige.size(); i++)
					{
						 cmbKnjigaIzborKataloskog.addItem(listaZaKomboBoxKataloskiKnjige.get(i));
					}
				}
				
			}
		});
		//
		cmbKnjigaIzborKataloskog.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				// TODO Auto-generated method stub
				if(brojac > 0)
				{
					if(arg0.getStateChange() == 1)
					{
						KonekcijaZaKnjige konekc = new KonekcijaZaKnjige();
						String cela = cmbKnjigaIzborKataloskog.getSelectedItem().toString();
						String nova = "";
						for(int i =0; i < cela.length(); i++)
						{
							char katakter = cela.charAt(i);
							if(katakter == '-')break;
							nova += katakter;
							
						}
						int n = Integer.parseInt(nova);
						Knjiga knjiga = konekc.SelectKnjigeNaOsnovuNazivaZanra(n);
						txtKnjigaKataloskiBroj.setText(""+ knjiga.getKataloskiBroj());
						txtKnjigaNaziv.setText(knjiga.getNaziv());
						txtKnjigaMesto.setText(knjiga.getMesto());
						
						if(txtKnjigaKataloskiBroj != null)
						{
							txtIzdanjaKataloskiBroj.setText(txtKnjigaKataloskiBroj.getText());
						}
					}
				}
				brojac++;
			}
		});
		
		JButton btnIzdanjeUnos = new JButton("UPISI");
		btnIzdanjeUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolorKnjige kontrola =new KontrolorKnjige();
				String poruka = kontrola.ProveraIzdanja(txtIzdanjaKataloskiBroj.getText(), txtIzdanjaDatum.getText(), txtIzdanjaISBN.getText(),
						txtIzdanjaBrojStrana.getText(), txtIzdanjaCena.getText(),
						txtIzdanjaBrojIzdanja.getText(), cmbIzdanjaIzborIzdavaca.getSelectedItem().toString(),
						cmbIzdanjaVez.getSelectedItem().toString());
				lblIzdanjaPoruka.setText(poruka);
				cmbAutoriIsbn.insertItemAt(txtIzdanjaISBN.getText(), 1);
				KonekcijaZaKnjige konekcione = new KonekcijaZaKnjige();
				ArrayList<String>listaZaBogacenjeKomboBoxaIsbnAutoriKnjiga = konekcione.BogacenjeKomboBoxaAutoriISBN();
				cmbAutoriIsbn.removeAllItems();
				for(int i = 0; i < listaZaBogacenjeKomboBoxaIsbnAutoriKnjiga.size(); i++)
				{
					cmbAutoriIsbn.addItem(listaZaBogacenjeKomboBoxaIsbnAutoriKnjiga.get(i));
				}
			}
		});
		btnIzdanjeUnos.setBounds(348, 329, 111, 34);
		panel_2.add(btnIzdanjeUnos);
		
		KonekcijaZaKnjige konekcione = new KonekcijaZaKnjige();
		ArrayList<String>listaZaBogacenjeKomboBoxaIzdavaciUizdanje = konekcione.BogacenjeKomboBoxaIzborIzdavacaKodIzdanja();
		for(int i =0; i < listaZaBogacenjeKomboBoxaIzdavaciUizdanje.size(); i++)
		{
			cmbIzdanjaIzborIzdavaca.addItem(listaZaBogacenjeKomboBoxaIzdavaciUizdanje.get(i));
		}
		
		ArrayList<String>listaZaBogacenjeKomboBoxaIsbnAutoriKnjiga = konekcione.BogacenjeKomboBoxaAutoriISBN();
		for(int i = 0; i < listaZaBogacenjeKomboBoxaIsbnAutoriKnjiga.size(); i++)
		{
			cmbAutoriIsbn.addItem(listaZaBogacenjeKomboBoxaIsbnAutoriKnjiga.get(i));
		}
		
		ArrayList<String>listaZaBogacenjeKomboBoxaAutori = konekcione.BogacenjeKomboBoxaAutorImenaAutora();
		for(int i = 0; i < listaZaBogacenjeKomboBoxaAutori.size(); i++)
		{
			cmbAutoriIzborAutora.addItem(listaZaBogacenjeKomboBoxaAutori.get(i));
		}


		

	}
}
