package Forme;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Kontroleri.KontrolerAutora;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AutoriForma extends JFrame {

	private JPanel contentPane;
	private JTextField txtIme;
	private JTextField txtPrezime;
	private JTextField txtDrazava;
	private JTextField txtGodinaRodjenja;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AutoriForma frame = new AutoriForma();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AutoriForma() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ime");
		lblNewLabel.setBounds(33, 32, 108, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Prezime");
		lblNewLabel_1.setBounds(33, 74, 108, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Drzava");
		lblNewLabel_2.setBounds(33, 120, 108, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Godina rodjenja");
		lblNewLabel_3.setBounds(33, 167, 108, 14);
		contentPane.add(lblNewLabel_3);
		
		txtIme = new JTextField();
		txtIme.setBounds(171, 29, 180, 20);
		contentPane.add(txtIme);
		txtIme.setColumns(10);
		
		txtPrezime = new JTextField();
		txtPrezime.setBounds(171, 71, 180, 20);
		contentPane.add(txtPrezime);
		txtPrezime.setColumns(10);
		
		txtDrazava = new JTextField();
		txtDrazava.setBounds(171, 117, 180, 20);
		contentPane.add(txtDrazava);
		txtDrazava.setColumns(10);
		
		txtGodinaRodjenja = new JTextField();
		txtGodinaRodjenja.setBounds(171, 164, 180, 20);
		contentPane.add(txtGodinaRodjenja);
		txtGodinaRodjenja.setColumns(10);
		
		JLabel lblPoruka = new JLabel("Poruka");
		lblPoruka.setBounds(35, 228, 227, 14);
		contentPane.add(lblPoruka);
		
		JButton btnUnos = new JButton("UNOS");
		btnUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolerAutora kontrola = new KontrolerAutora();
				String poruka = kontrola.ProveraUnosaAutora(txtIme.getText(), txtPrezime.getText(),
						txtDrazava.getText(), txtGodinaRodjenja.getText());
				lblPoruka.setText(poruka);
			}
		});
		btnUnos.setBounds(285, 220, 139, 31);
		contentPane.add(btnUnos);
		
		
	}
}
