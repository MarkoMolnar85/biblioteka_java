package Forme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import DAO.KonekcijaParametarskiUpit;
import DAO.KonekcijaUpiti;
import Entiteti.Citaoc;
import Entiteti.Knjiga;
import Entiteti.Rad;

public class ParametarskiUpitForma extends JFrame {

	private JPanel contentPane;
	String kojiUpit;
	JTable tabela;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ParametarskiUpitForma frame = new ParametarskiUpitForma("defolt1", "defolt2");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ParametarskiUpitForma(String text, String kojiU) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 863, 543);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		kojiUpit = kojiU;
		
		if(kojiUpit.equals("citaoci"))
		{
			try
			{
				KonekcijaParametarskiUpit konekcija = new KonekcijaParametarskiUpit();
				ArrayList<Citaoc>listaCitaoca = new ArrayList<Citaoc>();
				listaCitaoca = konekcija.CitaociNaOsnovuImenaIliPrezimenaSelect(text, text);
				
				String[] kolone = {"Broj clanske karte", "Ime", "Prezime", "Pol", "Datum rodjenja", "Drzava", "Grad", "Adresa"};
				Object[][] podaci = new Object[listaCitaoca.size()][8];
				
				for(int i = 0; i < listaCitaoca.size(); i++)
				{
					for(int j = 0; j < 8; j++)
					{
						podaci[i][0] = listaCitaoca.get(i).getBrojClanskeKarte();
						podaci[i][1] = listaCitaoca.get(i).getIme();
						podaci[i][2] = listaCitaoca.get(i).getPrezime();
						podaci[i][3] = listaCitaoca.get(i).getPol();
						podaci[i][4] = listaCitaoca.get(i).getGodinaRodjenja();
						podaci[i][5] = listaCitaoca.get(i).getDrzava();
						podaci[i][6] = listaCitaoca.get(i).getGrad();
						podaci[i][7] = listaCitaoca.get(i).getAdresa();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
			
			
		}
		
		
		if(kojiUpit.equals("knjige"))
		{
			try
			{
				KonekcijaParametarskiUpit konekcija = new KonekcijaParametarskiUpit();
				ArrayList<Knjiga>listaCitaoca = new ArrayList<Knjiga>();
				listaCitaoca = konekcija.KnjigaPoBiloKomOsnovu(text);
				
				String[] kolone = {"Kataloski broj", "Naziv", "Ostecenje", "Mesto", "Naziv zanra"};
				Object[][] podaci = new Object[listaCitaoca.size()][5];
				
				for(int i = 0; i < listaCitaoca.size(); i++)
				{
					for(int j = 0; j < 5; j++)
					{
						podaci[i][0] = listaCitaoca.get(i).getKataloskiBroj();
						podaci[i][1] = listaCitaoca.get(i).getNaziv();
						podaci[i][2] = listaCitaoca.get(i).getOstecenje();
						podaci[i][3] = listaCitaoca.get(i).getMesto();
						podaci[i][4] = listaCitaoca.get(i).getNazivZanra();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
		
		if(kojiUpit.equals("radovi"))
		{
			try
			{
				KonekcijaParametarskiUpit konekcija = new KonekcijaParametarskiUpit();
				ArrayList<Rad>listaCitaoca = new ArrayList<Rad>();
				listaCitaoca = konekcija.RadPoBiloKomOsnovu(text);
				
				String[] kolone = {"Naslov", "ISSN"};
				Object[][] podaci = new Object[listaCitaoca.size()][2];
				
				for(int i = 0; i < listaCitaoca.size(); i++)
				{
					for(int j = 0; j < 2; j++)
					{
						podaci[i][0] = listaCitaoca.get(i).getNaslov();
						podaci[i][1] = listaCitaoca.get(i).getIssn();

					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
	}

}
