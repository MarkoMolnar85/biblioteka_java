package Forme;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DAO.Konekcija;
import Entiteti.Citaoc;
import Kontroleri.Kontroler;

import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class CitaociForma extends JFrame {

	private JPanel contentPane;
	//private JFrame frame;
	private JTextField txtBrojLicneKarte;
	private JTextField txtIme;
	private JTextField txtPrezime;
	private JTextField txtDatumRodjenja;
	private JTextField txtDrzava;
	private JTextField txtGrad;
	private JTextField txtAdresa;
	String brojOdredjenogCitaoca;
	private JTextField txtTelefonBrojClanske;
	private JTextField txtTelefonIme;
	private JTextField txtTelefonBrojTelefona;
	private JTextField txtIzdavanjeDatuma;
	private JTextField txtIzdavanjeBrojClanske;
	private JTextField txtVracanjeDatum;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CitaociForma frame = new CitaociForma();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CitaociForma() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Citaoci", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblBrojLicneKarte = new JLabel("Broj clanske karte");
		lblBrojLicneKarte.setBounds(20, 27, 83, 14);
		panel.add(lblBrojLicneKarte);
		
		txtBrojLicneKarte = new JTextField();
		txtBrojLicneKarte.setBounds(113, 24, 232, 20);
		panel.add(txtBrojLicneKarte);
		txtBrojLicneKarte.setColumns(10);
		
		JLabel lblIzborCitaoca = new JLabel("Izbor citaoca");
		lblIzborCitaoca.setBounds(20, 66, 88, 14);
		panel.add(lblIzborCitaoca);
		
		JComboBox cmbIzborCitaoca = new JComboBox();
		cmbIzborCitaoca.setBounds(113, 63, 232, 20);
		panel.add(cmbIzborCitaoca);
		
		
		JLabel lblIme = new JLabel("Ime");
		lblIme.setBounds(20, 102, 46, 14);
		panel.add(lblIme);
		
		JLabel lblPrezime = new JLabel("Prezime");
		lblPrezime.setBounds(20, 137, 83, 14);
		panel.add(lblPrezime);
		
		JLabel lblDatumRodjenja = new JLabel("Datum rodjenja");
		lblDatumRodjenja.setBounds(20, 173, 83, 14);
		panel.add(lblDatumRodjenja);
		
		JLabel lblNewLabel = new JLabel("Drzava");
		lblNewLabel.setBounds(20, 208, 71, 14);
		panel.add(lblNewLabel);
		
		JLabel lblGrad = new JLabel("Grad");
		lblGrad.setBounds(20, 239, 71, 14);
		panel.add(lblGrad);
		
		JLabel lblNewLabel_1 = new JLabel("Adresa");
		lblNewLabel_1.setBounds(20, 270, 83, 14);
		panel.add(lblNewLabel_1);
		
		txtIme = new JTextField();
		txtIme.setBounds(111, 99, 234, 20);
		panel.add(txtIme);
		txtIme.setColumns(10);
		
		txtPrezime = new JTextField();
		txtPrezime.setBounds(113, 134, 232, 20);
		panel.add(txtPrezime);
		txtPrezime.setColumns(10);
		
		txtDatumRodjenja = new JTextField();
		txtDatumRodjenja.setBounds(113, 170, 232, 20);
		panel.add(txtDatumRodjenja);
		txtDatumRodjenja.setColumns(10);
		
		JLabel lblPol = new JLabel("Pol");
		lblPol.setBounds(20, 303, 46, 14);
		panel.add(lblPol);
		
		txtDrzava = new JTextField();
		txtDrzava.setBounds(113, 205, 232, 20);
		panel.add(txtDrzava);
		txtDrzava.setColumns(10);
		
		txtGrad = new JTextField();
		txtGrad.setBounds(113, 236, 232, 20);
		panel.add(txtGrad);
		txtGrad.setColumns(10);
		
		txtAdresa = new JTextField();
		txtAdresa.setBounds(113, 267, 232, 20);
		panel.add(txtAdresa);
		txtAdresa.setColumns(10);
		
		JLabel lblPoruka = new JLabel("Poruka:");
		lblPoruka.setBounds(20, 406, 325, 14);
		panel.add(lblPoruka);
		
		JComboBox cmbPol = new JComboBox();
		cmbPol.setModel(new DefaultComboBoxModel(new String[] {"musko", "zensko"}));
		cmbPol.setBounds(113, 300, 232, 20);
		panel.add(cmbPol);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Vracanje knjige", null, panel_3, null);
		panel_3.setLayout(null);
		
		JComboBox cmbVracanjeBrojClanske = new JComboBox();
		cmbVracanjeBrojClanske.setBounds(123, 62, 143, 20);
		panel_3.add(cmbVracanjeBrojClanske);
		
		JButton btnUnos = new JButton("Unos");
		btnUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Kontroler kontroler = new Kontroler();
				kontroler.ProveraCitaoca(txtBrojLicneKarte.getText(),
						txtIme.getText(), txtPrezime.getText(),
						txtDatumRodjenja.getText(), txtDrzava.getText(),
						txtGrad.getText(), txtAdresa.getText(), cmbPol.getSelectedItem().toString());
				lblPoruka.setText(kontroler.poruka);
				cmbIzborCitaoca.setSelectedItem(txtBrojLicneKarte.getText()); 
				
				//ponovo puni kombobox-ove
				cmbIzborCitaoca.addItem(txtBrojLicneKarte.getText() + "-" + txtIme.getText() + "-" + txtPrezime.getText());
				cmbVracanjeBrojClanske.addItem(txtBrojLicneKarte.getText() + "-" + txtIme.getText() + "-" + txtPrezime.getText());
				//cmbIzborCitaoca.removeAllItems();
				//cmbVracanjeBrojClanske.removeAllItems();
				/*Konekcija vratiCitaoce = new Konekcija();
				ArrayList<String>ListaZaComboBoxCitaoci = vratiCitaoce.SelectPopunjavanjeKomboBoxaCitaoci();
				if(ListaZaComboBoxCitaoci != null)
				for(int i =0; i < ListaZaComboBoxCitaoci.size(); i++)
				{
					cmbIzborCitaoca.addItem(ListaZaComboBoxCitaoci.get(i));
					cmbVracanjeBrojClanske.addItem(ListaZaComboBoxCitaoci.get(i));
					
				}*/
			}
		});
		btnUnos.setBounds(14, 335, 331, 46);
		panel.add(btnUnos);
		
		
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Broj telefona", null, panel_1, null);
		panel_1.setLayout(null);
		
		txtTelefonBrojClanske = new JTextField();
		txtTelefonBrojClanske.setBounds(144, 22, 181, 20);
		panel_1.add(txtTelefonBrojClanske);
		txtTelefonBrojClanske.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Broj clanske karte");
		lblNewLabel_2.setBounds(20, 22, 102, 14);
		panel_1.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Ime");
		lblNewLabel_3.setBounds(20, 75, 102, 14);
		panel_1.add(lblNewLabel_3);
		
		txtTelefonIme = new JTextField();
		txtTelefonIme.setBounds(144, 72, 181, 20);
		panel_1.add(txtTelefonIme);
		txtTelefonIme.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Broj telefona");
		lblNewLabel_4.setBounds(20, 127, 102, 14);
		panel_1.add(lblNewLabel_4);
		
		txtTelefonBrojTelefona = new JTextField();
		txtTelefonBrojTelefona.setBounds(144, 124, 181, 20);
		panel_1.add(txtTelefonBrojTelefona);
		txtTelefonBrojTelefona.setColumns(10);
		
		JLabel lblTelefonPoruka = new JLabel("Poruka");
		lblTelefonPoruka.setBounds(10, 216, 315, 14);
		panel_1.add(lblTelefonPoruka);
		
		JButton btnTelefonUnos = new JButton("UNOS");
		btnTelefonUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Kontroler kontr = new Kontroler();
				String poruka = kontr.ProveraTelefona(txtTelefonBrojClanske.getText()
						, txtTelefonIme.getText(), txtTelefonBrojTelefona.getText());
				lblTelefonPoruka.setText(poruka);
			}
		});
		btnTelefonUnos.setBounds(10, 166, 315, 33);
		panel_1.add(btnTelefonUnos);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Izdavanje knjige", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel_5 = new JLabel("Datuma uzeto");
		lblNewLabel_5.setBounds(25, 30, 106, 14);
		panel_2.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Broj clanske karte");
		lblNewLabel_6.setBounds(25, 83, 132, 14);
		panel_2.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Kataloski broj knjige");
		lblNewLabel_7.setBounds(25, 138, 133, 14);
		panel_2.add(lblNewLabel_7);
		
		txtIzdavanjeDatuma = new JTextField();
		txtIzdavanjeDatuma.setBounds(168, 27, 160, 20);
		panel_2.add(txtIzdavanjeDatuma);
		txtIzdavanjeDatuma.setColumns(10);
		
		txtIzdavanjeBrojClanske = new JTextField();
		txtIzdavanjeBrojClanske.setBounds(168, 80, 160, 20);
		panel_2.add(txtIzdavanjeBrojClanske);
		txtIzdavanjeBrojClanske.setColumns(10);
		
		JComboBox cmbIzdavanjeKataloskiBroj = new JComboBox();
		cmbIzdavanjeKataloskiBroj.setBounds(168, 135, 160, 20);
		panel_2.add(cmbIzdavanjeKataloskiBroj);
		
		JLabel lblIzdavanjePoruka = new JLabel("Poruka");
		lblIzdavanjePoruka.setEnabled(false);
		lblIzdavanjePoruka.setBounds(25, 237, 274, 14);
		
		JComboBox cmbVracanjeKataloskoBroj = new JComboBox();
		cmbVracanjeKataloskoBroj.setBounds(123, 105, 143, 20);
		panel_3.add(cmbVracanjeKataloskoBroj);
		
		JButton btnIzdavanjeUnos = new JButton("UNOS");
		btnIzdavanjeUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Kontroler kont = new Kontroler();
				String poruka = kont.ProveraIzdavanjaKnjige(txtIzdavanjeDatuma.getText(), txtIzdavanjeBrojClanske.getText(), 
						cmbIzdavanjeKataloskiBroj.getSelectedItem().toString());
				lblIzdavanjePoruka.setText(poruka);
				
				//popnov punjenje kataloskog broja za izdavanje
				cmbIzdavanjeKataloskiBroj.removeAllItems();
				Konekcija vratiKataloskeBrojeve = new Konekcija();
				ArrayList<String>ListaZaComboBoxKataloskiBrojevi = vratiKataloskeBrojeve.PopuluacijaKomboBoxaKataloskiBrojZaIzdavanjeKnjige();
				for(int i =0; i < ListaZaComboBoxKataloskiBrojevi.size(); i++)
				{
					cmbIzdavanjeKataloskiBroj.addItem(ListaZaComboBoxKataloskiBrojevi.get(i));
					
				}
				
				//ponovo popunjavanje kataloskog broja u vracanju
				cmbVracanjeKataloskoBroj.removeAllItems();
				Konekcija vratiNevraceneKnjige = new Konekcija();
				ArrayList<String>nevraceneKnjige =  vratiNevraceneKnjige.PopuluacijaKomboBoxaKataloskiBrojZaVracanjeKnjige();
				for(int i = 0; i < nevraceneKnjige.size(); i++)
				{
					cmbVracanjeKataloskoBroj.addItem(nevraceneKnjige.get(i));
				}
			}
		});
		btnIzdavanjeUnos.setBounds(25, 181, 132, 23);
		panel_2.add(btnIzdavanjeUnos);
		
		
		panel_2.add(lblIzdavanjePoruka);
		
		
		
		txtVracanjeDatum = new JTextField();
		txtVracanjeDatum.setBounds(123, 24, 143, 20);
		panel_3.add(txtVracanjeDatum);
		txtVracanjeDatum.setColumns(10);
		
		JLabel lblNewLabel_8 = new JLabel("Datum");
		lblNewLabel_8.setBounds(20, 24, 46, 14);
		panel_3.add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("Broj clanske karte");
		lblNewLabel_9.setBounds(20, 65, 91, 14);
		panel_3.add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("Kataloski broj");
		lblNewLabel_10.setBounds(20, 108, 91, 14);
		panel_3.add(lblNewLabel_10);
		
		JLabel lblVracanjePoruka = new JLabel("Poruka");
		lblVracanjePoruka.setBounds(20, 217, 246, 14);
		panel_3.add(lblVracanjePoruka);
		
		
		
		
		
		JButton btnVracanjeUpisi = new JButton("UNOS");
		btnVracanjeUpisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Kontroler kontr = new Kontroler();
				String poruka = kontr.ProveraVracanjaKnjige(txtVracanjeDatum.getText()
						,cmbVracanjeBrojClanske.getSelectedItem().toString(),
						cmbVracanjeKataloskoBroj.getSelectedItem().toString());
				lblVracanjePoruka.setText(poruka);
				
				//ponov popunjavanje kataloskog broja za izdavanje i vracanje knjiga
				
				cmbIzdavanjeKataloskiBroj.removeAllItems();
				Konekcija vratiKataloskeBrojeve = new Konekcija();
				ArrayList<String>ListaZaComboBoxKataloskiBrojevi = vratiKataloskeBrojeve.PopuluacijaKomboBoxaKataloskiBrojZaIzdavanjeKnjige();
				for(int i =0; i < ListaZaComboBoxKataloskiBrojevi.size(); i++)
				{
					cmbIzdavanjeKataloskiBroj.addItem(ListaZaComboBoxKataloskiBrojevi.get(i));
					
				}
				
				//ponovo popunjavanje kataloskog broja u vracanju
				cmbVracanjeKataloskoBroj.removeAllItems();
				Konekcija vratiNevraceneKnjige = new Konekcija();
				ArrayList<String>nevraceneKnjige =  vratiNevraceneKnjige.PopuluacijaKomboBoxaKataloskiBrojZaVracanjeKnjige();
				for(int i = 0; i < nevraceneKnjige.size(); i++)
				{
					cmbVracanjeKataloskoBroj.addItem(nevraceneKnjige.get(i));
				}
				
				
			}
		});
		btnVracanjeUpisi.setBounds(20, 152, 89, 35);
		panel_3.add(btnVracanjeUpisi);
		//////////////////// popunjavanje kombo boxa sa otvaranjem forme
		Konekcija vratiCitaoce = new Konekcija();
		ArrayList<String>ListaZaComboBoxCitaoci = vratiCitaoce.SelectPopunjavanjeKomboBoxaCitaoci();
		if(ListaZaComboBoxCitaoci != null)
		for(int i =0; i < ListaZaComboBoxCitaoci.size(); i++)
		{
			cmbIzborCitaoca.addItem(ListaZaComboBoxCitaoci.get(i));
			cmbVracanjeBrojClanske.addItem(ListaZaComboBoxCitaoci.get(i));
			
		}
		Konekcija vratiKataloskeBrojeve = new Konekcija();
		ArrayList<String>ListaZaComboBoxKataloskiBrojevi = vratiKataloskeBrojeve.PopuluacijaKomboBoxaKataloskiBrojZaIzdavanjeKnjige();
		for(int i =0; i < ListaZaComboBoxKataloskiBrojevi.size(); i++)
		{
			cmbIzdavanjeKataloskiBroj.addItem(ListaZaComboBoxKataloskiBrojevi.get(i));
			
		}
		cmbIzborCitaoca.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				// TODO Auto-generated method stub
				String sadrzaj = cmbIzborCitaoca.getSelectedItem().toString();
				String samoSamobroj = "";
				for(int i = 0; i < sadrzaj.length(); i++)
				{
					char karakter = sadrzaj.charAt(i);
					if(karakter == '-')
						break;
					samoSamobroj += karakter;
				}
				brojOdredjenogCitaoca = samoSamobroj;
				Konekcija kona = new Konekcija();
				Citaoc cit = kona.SELECTspecificniCItaoc(Integer.parseInt(brojOdredjenogCitaoca));
				txtBrojLicneKarte.setText("" + cit.getBrojClanskeKarte());
				txtIme.setText(cit.getIme());
				txtPrezime.setText(cit.getPrezime());
				txtDatumRodjenja.setText(cit.getGodinaRodjenja());
				txtDrzava.setText(cit.getDrzava());
				txtGrad.setText(cit.getGrad());
				txtAdresa.setText(cit.getAdresa());
				cmbPol.setSelectedItem(cit.getPol());
				
				txtTelefonBrojClanske.setText("" + cit.getBrojClanskeKarte());
				txtTelefonIme.setText(cit.getIme());
				txtIzdavanjeBrojClanske.setText("" + cit.getBrojClanskeKarte());
			}
	  });
		//
		Konekcija vratiNevraceneKnjige = new Konekcija();
		ArrayList<String>nevraceneKnjige =  vratiNevraceneKnjige.PopuluacijaKomboBoxaKataloskiBrojZaVracanjeKnjige();
		for(int i = 0; i < nevraceneKnjige.size(); i++)
		{
			cmbVracanjeKataloskoBroj.addItem(nevraceneKnjige.get(i));
		}
	}
}
