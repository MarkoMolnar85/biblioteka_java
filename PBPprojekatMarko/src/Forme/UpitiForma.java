package Forme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import DAO.KonekcijaUpiti;
import Entiteti.BrojKnjigaZaZanr;
import Entiteti.Citaoc;
import Entiteti.DrzavaIzdavacCasopisRad;
import Entiteti.IzdanjeSaAutorom;
import Entiteti.IzdatPrimerak;
import Entiteti.Knjiga;
import Entiteti.Telefon;

import java.awt.FlowLayout;

public class UpitiForma extends JFrame {

	private JPanel contentPane;
	String izborUpita;
	JTable tabela;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpitiForma frame = new UpitiForma("defolt");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpitiForma(String izborU) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 808, 504);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		izborUpita = izborU;
		
		if(izborUpita.equals("citaoci"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<Citaoc>listaCitaoca = new ArrayList<Citaoc>();
				listaCitaoca = konekcija.PregledCitaoca();
				
				String[] kolone = {"Broj clanske karte", "Ime", "Prezime", "Pol", "Datum rodjenja", "Drzava", "Grad", "Adresa"};
				Object[][] podaci = new Object[listaCitaoca.size()][8];
				
				for(int i = 0; i < listaCitaoca.size(); i++)
				{
					for(int j = 0; j < 8; j++)
					{
						podaci[i][0] = listaCitaoca.get(i).getBrojClanskeKarte();
						podaci[i][1] = listaCitaoca.get(i).getIme();
						podaci[i][2] = listaCitaoca.get(i).getPrezime();
						podaci[i][3] = listaCitaoca.get(i).getPol();
						podaci[i][4] = listaCitaoca.get(i).getGodinaRodjenja();
						podaci[i][5] = listaCitaoca.get(i).getDrzava();
						podaci[i][6] = listaCitaoca.get(i).getGrad();
						podaci[i][7] = listaCitaoca.get(i).getAdresa();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}//
		
		if(izborUpita.equals("telefoni"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<Telefon>lista = new ArrayList<Telefon>();
				lista = konekcija.PregledTelefona();
				
				String[] kolone = {"ID", "Broj telefona", "Broj clanske karte", "Ime"};
				Object[][] podaci = new Object[lista.size()][4];
				
				for(int i = 0; i < lista.size(); i++)
				{
					for(int j = 0; j < 4; j++)
					{
						podaci[i][0] = lista.get(i).getId();
						podaci[i][1] = lista.get(i).getBrojTelefona();
						podaci[i][2] = lista.get(i).getBrojClanskeKarte();
						podaci[i][3] = lista.get(i).getIme();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
		
		if(izborUpita.equals("izdate knjige"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<IzdatPrimerak>lista = new ArrayList<IzdatPrimerak>();
				lista = konekcija.IzdateKnjige();
				
				String[] kolone = {"Datuma uzeto", "Datuma vraceno", "Broj clanske karte", "Kataloski broj"};
				Object[][] podaci = new Object[lista.size()][4];
				
				for(int i = 0; i < lista.size(); i++)
				{
					for(int j = 0; j < 4; j++)
					{
						podaci[i][0] = lista.get(i).getDatumaUzeto();
						podaci[i][1] = lista.get(i).getDatumaVraceno();
						podaci[i][2] = lista.get(i).getBrojClanskeKarte();
						podaci[i][3] = lista.get(i).getKataloskiBroj();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
		
		if(izborUpita.equals("knjige"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<Knjiga>lista = new ArrayList<Knjiga>();
				lista = konekcija.Knjige();
				
				String[] kolone = {"Kataloski broj", "Naziv", "Ostecenje", "Mesto", "Zanr"};
				Object[][] podaci = new Object[lista.size()][5];
				
				for(int i = 0; i < lista.size(); i++)
				{
					for(int j = 0; j < 5; j++)
					{
						podaci[i][0] = lista.get(i).getKataloskiBroj();
						podaci[i][1] = lista.get(i).getNaziv();
						podaci[i][2] = lista.get(i).getOstecenje();
						podaci[i][3] = lista.get(i).getMesto();
						podaci[i][4] = lista.get(i).getNazivZanra();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
		
		if(izborUpita.equals("Izdanja i autori"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<IzdanjeSaAutorom>lista = new ArrayList<IzdanjeSaAutorom>();
				lista = konekcija.IzdanjaAutori();
				
				String[] kolone = {"Naziv knjige", "ISBN", "Uloga", "Prezime autora"};
				Object[][] podaci = new Object[lista.size()][4];
				
				for(int i = 0; i < lista.size(); i++)
				{
					for(int j = 0; j < 4; j++)
					{
						podaci[i][0] = lista.get(i).getNazivKnjige();
						podaci[i][1] = lista.get(i).getIsbn();
						podaci[i][2] = lista.get(i).getAutorKoautor();
						podaci[i][3] = lista.get(i).getPrezime();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
		if(izborUpita.equals("Casopisi radovi"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<DrzavaIzdavacCasopisRad>lista = new ArrayList<DrzavaIzdavacCasopisRad>();
				lista = konekcija.IzdavacCaspoisRad();
				
				String[] kolone = {"Drzava", "Naziv", "Issn", "Kataloski broj", "Urednik", "Naslov"};
				Object[][] podaci = new Object[lista.size()][6];
				
				for(int i = 0; i < lista.size(); i++)
				{
					for(int j = 0; j < 6; j++)
					{
						podaci[i][0] = lista.get(i).getDrzava();
						podaci[i][1] = lista.get(i).getNaziv();
						podaci[i][2] = lista.get(i).getIssn();
						podaci[i][3] = lista.get(i).getKataloskiBroj();
						podaci[i][4] = lista.get(i).getUrednik();
						podaci[i][5] = lista.get(i).getNaslov();
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
		
		if(izborUpita.equals("Broj knjiga po zanru"))
		{
			try
			{
				KonekcijaUpiti konekcija = new KonekcijaUpiti();
				ArrayList<BrojKnjigaZaZanr>lista = new ArrayList<BrojKnjigaZaZanr>();
				lista = konekcija.ZanroviBrojKnjigaSelect();
				
				String[] kolone = {"Zanr", "Broj knjiga"};
				Object[][] podaci = new Object[lista.size()][2];
				
				for(int i = 0; i < lista.size(); i++)
				{
					for(int j = 0; j < 2; j++)
					{
						podaci[i][0] = lista.get(i).getNazivZanra();
						podaci[i][1] = lista.get(i).getBrojKnjiga();
						
					}
				}
				
				tabela = new JTable(podaci, kolone);
				tabela.setPreferredScrollableViewportSize(new Dimension(this.getWidth() - 50, this.getHeight() - 50));
				tabela.setFillsViewportHeight(true);
				JScrollPane scrollPane = new JScrollPane(tabela);
				contentPane.add(scrollPane);
				tabela.getColumnModel().getColumn(0).setMinWidth(20);
				//tabela.getColumnModel().getColumn(1).setMinWidth(200);
			}
			catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
		}
	}//kraj glavne metode

}



















































