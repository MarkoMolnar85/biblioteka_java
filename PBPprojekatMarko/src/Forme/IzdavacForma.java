package Forme;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DAO.KonekcijaZaIzdavaca;
import Kontroleri.KontrolerZaIzdavaca;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class IzdavacForma extends JFrame {

	private JPanel contentPane;
	private JTextField txtNazivDrzave;
	private JTextField txtIzdavacNaziv;
	private JTextField txtIzdavacGrad;
	private JTextField txtIzdavacPostanski;
	private JTextField txtIzdavacTelefon;
	private JTextField txtIzdavacMail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IzdavacForma frame = new IzdavacForma();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IzdavacForma() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelDrzava = new JPanel();
		tabbedPane.addTab("Drzava", null, panelDrzava, null);
		panelDrzava.setLayout(null);
		
		JLabel lblDrzava = new JLabel("Drzava");
		lblDrzava.setBounds(10, 47, 46, 14);
		panelDrzava.add(lblDrzava);
		
		txtNazivDrzave = new JTextField();
		txtNazivDrzave.setBounds(84, 44, 257, 20);
		panelDrzava.add(txtNazivDrzave);
		txtNazivDrzave.setColumns(10);
		
		JLabel lblDrzavaPoruka = new JLabel("Poruka");
		lblDrzavaPoruka.setBounds(10, 422, 331, 14);
		panelDrzava.add(lblDrzavaPoruka);
		
		JPanel panel_izdavac = new JPanel();
		tabbedPane.addTab("Izdavac", null, panel_izdavac, null);
		panel_izdavac.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Naziv");
		lblNewLabel.setBounds(10, 31, 154, 14);
		panel_izdavac.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Grad");
		lblNewLabel_1.setBounds(10, 75, 154, 14);
		panel_izdavac.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Postanski broj");
		lblNewLabel_2.setBounds(10, 124, 154, 14);
		panel_izdavac.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Telefon");
		lblNewLabel_3.setBounds(10, 172, 154, 14);
		panel_izdavac.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Mail");
		lblNewLabel_4.setBounds(10, 211, 154, 14);
		panel_izdavac.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Ocena");
		lblNewLabel_5.setBounds(10, 256, 154, 14);
		panel_izdavac.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Drzava");
		lblNewLabel_6.setBounds(10, 298, 154, 14);
		panel_izdavac.add(lblNewLabel_6);
		
		JLabel lblIzdavacPoruka = new JLabel("Poruka");
		lblIzdavacPoruka.setBounds(10, 449, 399, 14);
		panel_izdavac.add(lblIzdavacPoruka);
		
		txtIzdavacNaziv = new JTextField();
		txtIzdavacNaziv.setBounds(208, 28, 201, 20);
		panel_izdavac.add(txtIzdavacNaziv);
		txtIzdavacNaziv.setColumns(10);
		
		txtIzdavacGrad = new JTextField();
		txtIzdavacGrad.setBounds(208, 72, 201, 20);
		panel_izdavac.add(txtIzdavacGrad);
		txtIzdavacGrad.setColumns(10);
		
		txtIzdavacPostanski = new JTextField();
		txtIzdavacPostanski.setBounds(208, 121, 201, 20);
		panel_izdavac.add(txtIzdavacPostanski);
		txtIzdavacPostanski.setColumns(10);
		
		txtIzdavacTelefon = new JTextField();
		txtIzdavacTelefon.setBounds(208, 169, 201, 20);
		panel_izdavac.add(txtIzdavacTelefon);
		txtIzdavacTelefon.setColumns(10);
		
		txtIzdavacMail = new JTextField();
		txtIzdavacMail.setBounds(208, 208, 201, 20);
		panel_izdavac.add(txtIzdavacMail);
		txtIzdavacMail.setColumns(10);
		
		JComboBox cmbIzdavacDrzava = new JComboBox();
		cmbIzdavacDrzava.setBounds(208, 295, 201, 20);
		panel_izdavac.add(cmbIzdavacDrzava);
		
		JComboBox cmbIzdavacOcena = new JComboBox();
		cmbIzdavacOcena.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		cmbIzdavacOcena.setBounds(208, 253, 201, 20);
		panel_izdavac.add(cmbIzdavacOcena);
		
		JButton btnIzdavacUnos = new JButton("UNOS");
		btnIzdavacUnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolerZaIzdavaca kontrola = new KontrolerZaIzdavaca();
				String poruka = kontrola.ProveraIzdavaca(txtIzdavacNaziv.getText(), txtIzdavacGrad.getText(),
						txtIzdavacPostanski.getText(), txtIzdavacTelefon.getText(), txtIzdavacMail.getText(),
						cmbIzdavacOcena.getSelectedItem().toString(), cmbIzdavacDrzava.getSelectedItem().toString());
				lblIzdavacPoruka.setText(poruka);
			}
		});
		btnIzdavacUnos.setBounds(420, 432, 139, 31);
		panel_izdavac.add(btnIzdavacUnos);
		
		JButton btnUpisiDrzavu = new JButton("UPIS");
		btnUpisiDrzavu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				KontrolerZaIzdavaca kontroler = new KontrolerZaIzdavaca();
				String poruka = kontroler.ProveraDrzave(txtNazivDrzave.getText());
				lblDrzavaPoruka.setText(poruka);
				
				KonekcijaZaIzdavaca kona = new KonekcijaZaIzdavaca();
				int kljuc = kona.SledeciIdDrzave();
				try
				{
				String strZaKombo = "" + kljuc + "-" + txtNazivDrzave.getText();
				cmbIzdavacDrzava.insertItemAt(strZaKombo, 0);
				}
				catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
				}
		});
		btnUpisiDrzavu.setBounds(382, 43, 119, 23);
		panelDrzava.add(btnUpisiDrzavu);
		
		KonekcijaZaIzdavaca kone = new KonekcijaZaIzdavaca();
		ArrayList<String>listaZaBogacenjaKomboBoxaDrzave = kone.ObogacivanjeKomboBoxaIzdavacaDrzava();
		for(int i =0; i < listaZaBogacenjaKomboBoxaDrzave.size(); i++)
		{
			cmbIzdavacDrzava.addItem(listaZaBogacenjaKomboBoxaDrzave.get(i));
		}
	}
	
	
}
