package Forme;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PitalicaForma extends JFrame {

	private JPanel contentPane;
	private JTextField txtParametar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PitalicaForma frame = new PitalicaForma("def");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PitalicaForma(String kojiUpit) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 357, 135);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Uneti parametar");
		lblNewLabel.setBounds(10, 22, 93, 14);
		contentPane.add(lblNewLabel);
		
		txtParametar = new JTextField();
		txtParametar.setBounds(127, 19, 204, 20);
		contentPane.add(txtParametar);
		txtParametar.setColumns(10);
		
		JButton btnUnosParametra = new JButton("Prikazi");
		btnUnosParametra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtParametar.getText() != null && txtParametar.getText().length()>0)
				{
					ParametarskiUpitForma frm = new ParametarskiUpitForma(txtParametar.getText(), kojiUpit);
					frm.setVisible(true);
				}
			}
		});
		btnUnosParametra.setBounds(127, 47, 204, 28);
		contentPane.add(btnUnosParametra);
		
		
	}
}
