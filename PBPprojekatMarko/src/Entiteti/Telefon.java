package Entiteti;

public class Telefon {
	private int id;
	private String brojTelefona;
	private int brojClanskeKarte;
	private String ime;
	public Telefon(){}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBrojTelefona() {
		return brojTelefona;
	}
	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	public int getBrojClanskeKarte() {
		return brojClanskeKarte;
	}
	public void setBrojClanskeKarte(int brojClanskeKarte) {
		this.brojClanskeKarte = brojClanskeKarte;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Telefon(int id, String brojTelefona, int brojClanskeKarte, String ime) {
		super();
		this.id = id;
		this.brojTelefona = brojTelefona;
		this.brojClanskeKarte = brojClanskeKarte;
		this.ime = ime;
	}

}