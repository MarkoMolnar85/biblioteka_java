package Entiteti;

public class Knjiga {
	private int kataloskiBroj;
	private String naziv;
	private String ostecenje;
	private	String mesto;
	private int idZanra;
	private String nazivZanra;
	public Knjiga(int kataloskiBroj, String naziv, String ostecenje, String mesto, int idZanra, String nazivZanra) {
		super();
		this.kataloskiBroj = kataloskiBroj;
		this.naziv = naziv;
		this.ostecenje = ostecenje;
		this.mesto = mesto;
		this.idZanra = idZanra;
		this.nazivZanra = nazivZanra;
	}
	public Knjiga(){}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOstecenje() {
		return ostecenje;
	}
	public void setOstecenje(String ostecenje) {
		this.ostecenje = ostecenje;
	}
	public String getMesto() {
		return mesto;
	}
	public void setMesto(String mesto) {
		this.mesto = mesto;
	}
	public int getIdZanra() {
		return idZanra;
	}
	public void setIdZanra(int idZanra) {
		this.idZanra = idZanra;
	}
	public String getNazivZanra() {
		return nazivZanra;
	}
	public void setNazivZanra(String nazivZanra) {
		this.nazivZanra = nazivZanra;
	}
	
}
