package Entiteti;

public class Izdavac {
	private int id;
	private String naziv;
	private String grad;
	private String postanskiBroj;
	private String telefon;
	private String mail;
	private int ocena;
	private int idDrzava;
	private String nazivDrzave;
	
	public String getNazivDrzave() {
		return nazivDrzave;
	}
	public void setNazivDrzave(String nazivDrzave) {
		this.nazivDrzave = nazivDrzave;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getPostanskiBroj() {
		return postanskiBroj;
	}
	public void setPostanskiBroj(String postanskiBroj) {
		this.postanskiBroj = postanskiBroj;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public int getOcena() {
		return ocena;
	}
	public void setOcena(int ocena) {
		this.ocena = ocena;
	}
	public int getIdDrzava() {
		return idDrzava;
	}
	public void setIdDrzava(int idDrzava) {
		this.idDrzava = idDrzava;
	}
	public Izdavac(int id, String naziv, String grad, String postanskiBroj, String telefon, String mail, int ocena,
			int idDrzava, String nazivDrzave) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.grad = grad;
		this.postanskiBroj = postanskiBroj;
		this.telefon = telefon;
		this.mail = mail;
		this.ocena = ocena;
		this.idDrzava = idDrzava;
		this.nazivDrzave = nazivDrzave;
	}

}
