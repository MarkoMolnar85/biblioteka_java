package Entiteti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Citaoc {
	private int brojClanskeKarte;
	private String ime;
	private String prezime;
	private String pol;
	private String godinaRodjenja;
	private String drzava;
	public Citaoc(int brojClanskeKarte, String ime, String prezime, String pol, String godinaRodjenja, String drzava,
			String grad, String adresa) {
		super();
		this.brojClanskeKarte = brojClanskeKarte;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.godinaRodjenja = godinaRodjenja;
		this.drzava = drzava;
		this.grad = grad;
		this.adresa = adresa;
	}
	public Citaoc(){}
	public int getBrojClanskeKarte() {
		return brojClanskeKarte;
	}
	public void setBrojClanskeKarte(int brojClanskeKarte) {
		this.brojClanskeKarte = brojClanskeKarte;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getGodinaRodjenja() {
		return godinaRodjenja;
	}
	public void setGodinaRodjenja(String godinaRodjenja) {
		this.godinaRodjenja = godinaRodjenja;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	private String grad;
	private String adresa;
	
	
}
