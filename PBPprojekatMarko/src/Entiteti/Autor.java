package Entiteti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Autor {
	private int id;
	private String ime;
	private String prezime;
	private String drzava;
	private String datumRodjenja;
	public Autor(int id, String ime, String prezime, String drzava, String datumRodjenja) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.drzava = drzava;
		this.datumRodjenja = datumRodjenja;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public String getDatumRodjenja() {
		return datumRodjenja;
	}
	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}
	
	

}
