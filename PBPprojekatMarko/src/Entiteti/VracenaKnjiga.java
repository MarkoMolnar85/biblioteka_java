package Entiteti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VracenaKnjiga {
	private String datumaVraceno;
	private int brojClanskeKarte;
	private int kataloskiBroj;
	public String getDatumaVraceno() {
		return datumaVraceno;
	}
	public void setDatumaVraceno(String datumaVraceno) {
		this.datumaVraceno = datumaVraceno;
	}
	public int getBrojClanskeKarte() {
		return brojClanskeKarte;
	}
	public void setBrojClanskeKarte(int brojClanskeKarte) {
		this.brojClanskeKarte = brojClanskeKarte;
	}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	public VracenaKnjiga(String datumaVraceno, int brojClanskeKarte, int kataloskiBroj) {
		super();
		this.datumaVraceno = datumaVraceno;
		this.brojClanskeKarte = brojClanskeKarte;
		this.kataloskiBroj = kataloskiBroj;
	}
	
	
	
}
