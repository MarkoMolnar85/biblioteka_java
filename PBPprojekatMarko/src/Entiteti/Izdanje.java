package Entiteti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Izdanje {
	private String datum;
	private String isbn;
	private int brojStrana;
	private double cena;
	private int brojIzdanja;
	private String vez;
	private int kataloskiBroj;
	private int izdavac;
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getBrojStrana() {
		return brojStrana;
	}
	public void setBrojStrana(int brojStrana) {
		this.brojStrana = brojStrana;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public int getBrojIzdanja() {
		return brojIzdanja;
	}
	public void setBrojIzdanja(int brojIzdanja) {
		this.brojIzdanja = brojIzdanja;
	}
	public String getVez() {
		return vez;
	}
	public void setVez(String vez) {
		this.vez = vez;
	}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	public int getIzdavac() {
		return izdavac;
	}
	public void setIzdavac(int izdavac) {
		this.izdavac = izdavac;
	}
	public Izdanje(String datum, String isbn, int brojStrana, double cena, int brojIzdanja, String vez,
			int kataloskiBroj, int izdavac) {
		super();
		this.datum = datum;
		this.isbn = isbn;
		this.brojStrana = brojStrana;
		this.cena = cena;
		this.brojIzdanja = brojIzdanja;
		this.vez = vez;
		this.kataloskiBroj = kataloskiBroj;
		this.izdavac = izdavac;
	}
	
	
}
