package Entiteti;

public class BrojKnjigaZaZanr {
	
	private int brojKnjiga;
	String nazivZanra;
	public BrojKnjigaZaZanr(int brojKnjiga, String nazivZanra) {
		super();
		this.brojKnjiga = brojKnjiga;
		this.nazivZanra = nazivZanra;
	}
	public int getBrojKnjiga() {
		return brojKnjiga;
	}
	public void setBrojKnjiga(int brojKnjiga) {
		this.brojKnjiga = brojKnjiga;
	}
	public String getNazivZanra() {
		return nazivZanra;
	}
	public void setNazivZanra(String nazivZanra) {
		this.nazivZanra = nazivZanra;
	}
	public BrojKnjigaZaZanr(){}
}
