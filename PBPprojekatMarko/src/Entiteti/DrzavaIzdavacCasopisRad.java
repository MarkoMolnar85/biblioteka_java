package Entiteti;

public class DrzavaIzdavacCasopisRad {
	private String drzava;
	private String naziv;
	private String issn;
	private int kataloskiBroj;
	private String urednik;
	private String naslov;
	public DrzavaIzdavacCasopisRad(String drzava, String naziv, String issn, int kataloskiBroj, String urednik,
			String naslov) {
		super();
		this.drzava = drzava;
		this.naziv = naziv;
		this.issn = issn;
		this.kataloskiBroj = kataloskiBroj;
		this.urednik = urednik;
		this.naslov = naslov;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getIssn() {
		return issn;
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	public String getUrednik() {
		return urednik;
	}
	public void setUrednik(String urednik) {
		this.urednik = urednik;
	}
	public String getNaslov() {
		return naslov;
	}
	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}
	public DrzavaIzdavacCasopisRad(){}
}
