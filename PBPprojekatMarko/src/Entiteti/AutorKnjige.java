package Entiteti;

public class AutorKnjige {
	private String autorKoautor;
	private String isbn;
	private int id;
	public String getAutorKoautor() {
		return autorKoautor;
	}
	public void setAutorKoautor(String autorKoautor) {
		this.autorKoautor = autorKoautor;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public AutorKnjige(String autorKoautor, String isbn, int id) {
		super();
		this.autorKoautor = autorKoautor;
		this.isbn = isbn;
		this.id = id;
	}
	

}
