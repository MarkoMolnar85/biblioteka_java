package Entiteti;

//import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class IzdatPrimerak {
	private String datumaUzeto;
	private String datumaVraceno;
	private int brojClanskeKarte;
	private int kataloskiBroj;
	public IzdatPrimerak(){}
	public IzdatPrimerak(String datumaUzeto, String datumaVraceno, int brojClanskeKarte, int kataloskiBroj) {
		super();
		this.datumaUzeto = datumaUzeto;
		this.datumaVraceno = datumaVraceno;
		this.brojClanskeKarte = brojClanskeKarte;
		this.kataloskiBroj = kataloskiBroj;
	}
	public String getDatumaUzeto() {
		 return datumaUzeto;
	}
	public void setDatumaUzeto(String datumaUzeto) {
		this.datumaUzeto = datumaUzeto;
	}
	public String getDatumaVraceno() {
		return datumaVraceno;
	}
	public void setDatumaVraceno(String datumaVraceno) {
		this.datumaVraceno = datumaVraceno;
	}
	public int getBrojClanskeKarte() {
		return brojClanskeKarte;
	}
	public void setBrojClanskeKarte(int brojClanskeKarte) {
		this.brojClanskeKarte = brojClanskeKarte;
	}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	
	
	
}






























