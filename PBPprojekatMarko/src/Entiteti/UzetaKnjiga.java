package Entiteti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UzetaKnjiga {
	private String datumauzeto;
	private int brojClanskeKarte;
	int kataloskiBroj;
	public UzetaKnjiga(String datumauzeto, int brojClanskeKarte, int kataloskiBroj) {
		super();
		this.datumauzeto = datumauzeto;
		this.brojClanskeKarte = brojClanskeKarte;
		this.kataloskiBroj = kataloskiBroj;
	}
	public UzetaKnjiga(){}
	public String getDatumauzeto() {
		return datumauzeto;
	}
	public void setDatumauzeto(String datumauzeto) {
		this.datumauzeto = datumauzeto;
	}
	public int getBrojClanskeKarte() {
		return brojClanskeKarte;
	}
	public void setBrojClanskeKarte(int brojClanskeKarte) {
		this.brojClanskeKarte = brojClanskeKarte;
	}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	
	
}
