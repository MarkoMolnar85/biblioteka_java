package Entiteti;

public class IzdanjeSaAutorom {
	private String nazivKnjige;
	private String isbn;
	public IzdanjeSaAutorom(String nazivKnjige, String isbn, String autorKoautor, String prezime) {
		super();
		this.nazivKnjige = nazivKnjige;
		this.isbn = isbn;
		this.autorKoautor = autorKoautor;
		this.prezime = prezime;
	}
	public String getNazivKnjige() {
		return nazivKnjige;
	}
	public void setNazivKnjige(String nazivKnjige) {
		this.nazivKnjige = nazivKnjige;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getAutorKoautor() {
		return autorKoautor;
	}
	public void setAutorKoautor(String autorKoautor) {
		this.autorKoautor = autorKoautor;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	private String autorKoautor;
	private String prezime;
	public IzdanjeSaAutorom(){}
}
