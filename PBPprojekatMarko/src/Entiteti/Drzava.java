package Entiteti;

public class Drzava {
	private int id;
	private String drzava;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public Drzava(int id, String drzava) {
		super();
		this.id = id;
		this.drzava = drzava;
	}
	

}
