package Entiteti;

public class Rad {
	private String naslov;
	private String issn;
	private int idAutora;
	public Rad(String naslov, String issn, int idAutora) {
		super();
		this.naslov = naslov;
		this.issn = issn;
		this.idAutora = idAutora;
	}
	public String getNaslov() {
		return naslov;
	}
	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}
	public String getIssn() {
		return issn;
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public int getIdAutora() {
		return idAutora;
	}
	public void setIdAutora(int idAutora) {
		this.idAutora = idAutora;
	}
	public Rad(){}
}
