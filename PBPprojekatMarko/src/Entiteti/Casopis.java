package Entiteti;

public class Casopis {
	private String issn;
	private String naslov;
	private int kataloskiBroj;
	private String urednik;
	private int izdavac;
	public Casopis(String issn, String naslov, int kataloskiBroj, String urednik, int izdavac) {
		super();
		this.issn = issn;
		this.naslov = naslov;
		this.kataloskiBroj = kataloskiBroj;
		this.urednik = urednik;
		this.izdavac = izdavac;
	}
	public String getIssn() {
		return issn;
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public String getNaslov() {
		return naslov;
	}
	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}
	public int getKataloskiBroj() {
		return kataloskiBroj;
	}
	public void setKataloskiBroj(int kataloskiBroj) {
		this.kataloskiBroj = kataloskiBroj;
	}
	public String getUrednik() {
		return urednik;
	}
	public void setUrednik(String urednik) {
		this.urednik = urednik;
	}
	public int getIzdavac() {
		return izdavac;
	}
	public void setIzdavac(int izdavac) {
		this.izdavac = izdavac;
	}
}
