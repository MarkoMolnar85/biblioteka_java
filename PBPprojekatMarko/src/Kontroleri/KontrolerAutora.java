package Kontroleri;

import javax.swing.JOptionPane;

import DAO.KonekcijaZaAutor;
import Entiteti.Autor;

public class KontrolerAutora {

	private String UmanjenjeDoBroja(String textic)
	{
		String skracen="";
		for(int i = 0; i < textic.length(); i++)
		{
			char karakter = textic.charAt(i);
			if(karakter == '-')
				break;
			skracen += karakter;
		}
		return skracen;
	}
	private boolean ProveraDatuma(String datum)
	{
		boolean povratna  = true;
		if(datum != null && datum.length() > 0)
		{
			char karakter = ' ';
			try
			{
				int broj= 0;
				karakter = datum.charAt(0);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(1);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(3);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(4);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(6);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(7);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(8);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(9);
				broj = Integer.parseInt("" + karakter);
			}
			catch(Exception e)
			{
				povratna = false;
			}
			karakter = datum.charAt(2);
			if(karakter != '/')
				povratna = false;
			karakter = datum.charAt(5);
			if(karakter != '/')
				povratna = false;
		}
		else
			povratna = false;
		
		return povratna;
	}
	
	public String ProveraUnosaAutora(String ime, String prezime, String drzava, String datum)
	{
		KonekcijaZaAutor konekcija = new KonekcijaZaAutor();
		String poruka = "Novi autor je memorisan";
		int id = konekcija.SledeciIdAutora();
		if(ime != null && ime.length() > 0)
		{
			if(prezime != null && prezime.length() > 0)
			{
				if(drzava != null && drzava.length() > 0)
				{
					if(ProveraDatuma(datum))
					{			
						Autor autor = new Autor(id, ime, prezime, drzava, datum);
						konekcija.InsertAutora(autor);
					}
					else
						poruka = "Potrebno je uneti ispravan datum";
				}
				else
					poruka = "Potrebno je uneti drzavu autora";
			}
			else
				poruka = "Potrebno je uneti prezime";
		}
		else
			poruka = "Potrebno je uneti ime";
		
		return poruka;
	}
	
	
	
	
	
	
	
	
	
	
	
}
