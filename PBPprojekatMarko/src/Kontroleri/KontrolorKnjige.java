package Kontroleri;

import javax.swing.JOptionPane;

import DAO.KonekcijaZaKnjige;
import Entiteti.AutorKnjige;
import Entiteti.Izdanje;
import Entiteti.Knjiga;
import Entiteti.Zanr;

public class KontrolorKnjige {
	private String UmanjenjeDoBroja(String textic)
	{
		String skracen="";
		for(int i = 0; i < textic.length(); i++)
		{
			char karakter = textic.charAt(i);
			if(karakter == '-')
				break;
			skracen += karakter;
		}
		return skracen;
	}
	private boolean ProveraDatuma(String datum)
	{
		boolean povratna  = true;
		char karakter = ' ';
		if(datum != null && datum.length() > 0)
		{
			try
			{
				int broj= 0;
				karakter = datum.charAt(0);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(1);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(3);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(4);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(6);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(7);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(8);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(9);
				broj = Integer.parseInt("" + karakter);
			}
			catch(Exception e)
			{
				povratna = false;
			}
			karakter = datum.charAt(2);
			if(karakter != '/')
				povratna = false;
			karakter = datum.charAt(5);
			if(karakter != '/')
				povratna = false;
		}
		else
			povratna = false;
		
		return povratna;
	}
	public String ProveraZanra(String nazivZanra)
	{
		String poruka = "Novi zanr je unet";
		if(nazivZanra != null && nazivZanra.length() > 0)
		{
			KonekcijaZaKnjige kona = new KonekcijaZaKnjige();
			int n = kona.MaxIDZanra();
			++n;
			Zanr zanr = new Zanr(n, nazivZanra);
			kona.InsertovanejZanra(zanr);
		}
		else
			poruka = "Potrebno je uneti naziv zanra";
		return poruka;
	}
	
	public String ProveraUpisaKnjige(String nazivZanra, String kataloskiBroj, String nazivKnjige, String ostecenje, String mesto)
	{
		String poruka = "Nova knjiga je upisana";
		KonekcijaZaKnjige kona = new KonekcijaZaKnjige();
		if(nazivZanra != null && nazivZanra.length() > 0)
		{
			int idZanra = kona.VracaIDzanraNaOsnovuNazivaZanra(nazivZanra);
			try
			{
				int kb = Integer.parseInt(kataloskiBroj);
				if(nazivKnjige != null && nazivKnjige.length() > 0)
				{
					if(ostecenje != null && ostecenje.length() > 0)
					{
						if(mesto != null && mesto.length() > 0)
						{
							Knjiga knjiga = new Knjiga(kb, nazivKnjige, ostecenje, mesto, idZanra, nazivZanra);
							kona.InsertovanjeKnjige(knjiga);
						}
						else
						poruka = "Mesto mora biti uneto";
					}
					else
					poruka = "Pitanje ostecenja mora biti uneto";
				}
				else
				poruka = "Naziv knjige mora biti unet";
			}
			catch(Exception ex)
			{
				poruka = "Katloski broj mora biti broj";
			}
		}
		else
		{
			poruka = "Potrebno je uneti naziv zanra";
		}
		return poruka;
	}
	
	public String ProveraIzdanja(String kataloskiBroj, String datum, String isbn, String brojStrana, String cena, 
			String brojIzdanja, String izdavac, String vez)
	{
		String izdavacSkracen = UmanjenjeDoBroja(izdavac);
		String poruka = "Novo izdanje je memorisano";
		try
		{
			int kataloskiBr = Integer.parseInt(kataloskiBroj);
			if(ProveraDatuma(datum))
			{
				if(isbn != null && isbn.length() > 0)
				{
					try
					{
						int brStrana = Integer.parseInt(brojStrana);
						try{
							double cenaIzdanja = Double.parseDouble(cena);
							try{
								int brIzdanja = Integer.parseInt(brojIzdanja);
								try{
									int izdavacKey = Integer.parseInt(izdavacSkracen);
									KonekcijaZaKnjige kona = new KonekcijaZaKnjige();
									Izdanje izd = new Izdanje(datum, isbn, brStrana, cenaIzdanja, brIzdanja, vez, kataloskiBr, izdavacKey);
									kona.InsertovanjeIzdanja(izd);
									
								}
								catch(Exception ex)
								{
									poruka = "Izdavac nije ispravno unet";
								}
							}
							catch(Exception ex)
							{
								poruka = "Broj izdanja mora biti broj";
							}
						}
						catch(Exception ex)
						{
							poruka = "Cena mora biti broj";
						}
					}
					catch(Exception ex)
					{
						poruka = "Broj strana mora biti broj";
					}
				}
				else
					poruka = "Potrebno uneti isbn";
			}
			else
				poruka = "Potrebno je ispravno upisati datum";
		}
		catch(Exception ex)
		{
			poruka = "Kataloski broj mora biti broj";
		}
		
		return poruka;
	}
	
	public String ProveraUnosaAutoraZaKnjige(String status, String isbn, String autor)
	{
		String poruka = "Autor/koautor za ovo izdanje je memorisan";
		autor = UmanjenjeDoBroja(autor);
		int intAutor = Integer.parseInt(autor);
		AutorKnjige autorKnjige = new AutorKnjige(status, isbn, intAutor);
		KonekcijaZaKnjige konek = new KonekcijaZaKnjige();
		konek.InsertAutoraKnjige(autorKnjige);
		
		return poruka;
	}
	
	
	
	
	
	
	
	
	
	

}
