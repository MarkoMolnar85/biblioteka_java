package Kontroleri;

import DAO.KonekcijaZaCasopis;
import Entiteti.Casopis;
import Entiteti.Rad;

public class KontrolerZaCasopis {
	private String UmanjenjeDoBroja(String textic)
	{
		String skracen="";
		for(int i = 0; i < textic.length(); i++)
		{
			char karakter = textic.charAt(i);
			if(karakter == '-')
				break;
			skracen += karakter;
		}
		return skracen;
	}
	private boolean ProveraDatuma(String datum)
	{
		boolean povratna  = true;
		if(datum != null && datum.length() > 0)
		{
			char karakter = ' ';
			try
			{
				int broj= 0;
				karakter = datum.charAt(0);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(1);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(3);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(4);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(6);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(7);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(8);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(9);
				broj = Integer.parseInt("" + karakter);
			}
			catch(Exception e)
			{
				povratna = false;
			}
			karakter = datum.charAt(2);
			if(karakter != '/')
				povratna = false;
			karakter = datum.charAt(5);
			if(karakter != '/')
				povratna = false;
		}
		else
			povratna = false;
		
		return povratna;
	}
	
	public String ProveraCasopisa(String issn, String naslov, String kataloskiBroj, String urednik, String izdavac)
	{
		int izdavacId = Integer.parseInt(UmanjenjeDoBroja(izdavac));
		String poruka = "Novi casopis je memorisan";
		if(issn !=null && issn.length() > 0)
		{
			if(naslov !=null && naslov.length() > 0)
			{
				try{
					int kataloskiBr = Integer.parseInt(kataloskiBroj);
					if(urednik !=null && urednik.length() > 0)
					{
						Casopis casopis = new Casopis(issn, naslov, kataloskiBr, urednik, izdavacId);
						KonekcijaZaCasopis kona = new KonekcijaZaCasopis();
						kona.InsertovanjeCasopisa(casopis);
					}
					else
						poruka = "Potrebno je uneti urednika";
				}
				catch(Exception ex){poruka = "kataloski broj treba da bude unesen kao broj";}
			}
			else
				poruka = "Potrebno je naslov";
		}
		else
			poruka = "Potrebno je uneti issn ovog casopisa";
		
		return poruka;
	}
	
	public String ProveraRada(String naslov, String issn, String autor)
	{
		String poruka = "Novi rad je memorisan";
		int kljucAutora = Integer.parseInt(UmanjenjeDoBroja(autor));
		if(naslov !=null && naslov.length() > 0)
		{
			if(issn !=null && issn.length() > 0)
			{
				Rad rad = new Rad(naslov, issn, kljucAutora);
				KonekcijaZaCasopis kona = new KonekcijaZaCasopis();
				kona.InsertovanjeRadova(rad);
			}
			else
				poruka = "Potrebno je uneti issn";
		}
			else
				poruka = "Potrebno je uneti naslov";
		return poruka;
	}
	
	
	
	
	
	
	
	
	
	
	
}
