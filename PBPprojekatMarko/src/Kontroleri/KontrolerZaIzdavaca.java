package Kontroleri;

import DAO.KonekcijaZaIzdavaca;
import Entiteti.Drzava;
import Entiteti.Izdavac;

public class KontrolerZaIzdavaca {
	
	private String PosleCrteDoKraja(String subjekat)
	{
		String strPosleCrte = "";
		int brojac = 0;
		for(int i = 0; i < subjekat.length(); i++)
		{
			char karakter = subjekat.charAt(i);
			if(brojac == 1)
			{
				strPosleCrte +=karakter;
			}
			if(karakter == '-')
				brojac++;	
		}
		return strPosleCrte;
	}

	private String UmanjenjeDoBroja(String textic)
	{
		String skracen="";
		for(int i = 0; i < textic.length(); i++)
		{
			char karakter = textic.charAt(i);
			if(karakter == '-')
				break;
			skracen += karakter;
		}
		return skracen;
	}
	private boolean ProveraDatuma(String datum)
	{
		boolean povratna  = true;
		if(datum != null && datum.length() > 0)
		{
			char karakter = ' ';
			try
			{
				int broj= 0;
				karakter = datum.charAt(0);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(1);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(3);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(4);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(6);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(7);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(8);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(9);
				broj = Integer.parseInt("" + karakter);
			}
			catch(Exception e)
			{
				povratna = false;
			}
			karakter = datum.charAt(2);
			if(karakter != '/')
				povratna = false;
			karakter = datum.charAt(5);
			if(karakter != '/')
				povratna = false;
		}
		else
			povratna = false;
		
		return povratna;
	}
	
	public String ProveraDrzave(String imeDrzave)
	{
		KonekcijaZaIzdavaca konekcija = new KonekcijaZaIzdavaca();
		String poruka = "Nova drzava je memorisana";
		int n = konekcija.SledeciIdDrzave();
		if(imeDrzave != null && imeDrzave.length() > 0)
		{
			Drzava drzava = new Drzava(n, imeDrzave);
			konekcija.InserovanjeDrzave(drzava);
		}
		else
			poruka = "Potrebno je uneti ime drzave";
		
		return poruka;
	}
	
	public String ProveraIzdavaca(String naziv, String grad, String postanskiBroj, String telefon, String mail, String ocena, String drzava)
	{
		String poruka = "Novi izdavac je memorisan";
		KonekcijaZaIzdavaca konekcija = new KonekcijaZaIzdavaca();
		int idDrzave = Integer.parseInt(UmanjenjeDoBroja(drzava));
		String imeDrzaveS = PosleCrteDoKraja(drzava);
		int id = konekcija.SledeciIDIzdvaca();
		if(naziv != null && naziv.length() > 0)
		{

			if(grad != null && grad.length() > 0)
			{
				if(postanskiBroj != null && postanskiBroj.length() > 0)
				{
					if(telefon != null && telefon.length() > 0)
					{
						if(mail != null && mail.length() > 0)
						{
								try{
									int ocenaS = Integer.parseInt(ocena);
									if(ocenaS >= 1 || ocenaS <= 5)
									{
										if(drzava != null && drzava.length() > 0)
										{
											Izdavac izdavac = new Izdavac(id, naziv, grad, postanskiBroj, telefon, mail, ocenaS, idDrzave, imeDrzaveS);
											konekcija.InsertovanjeIzdavaca(izdavac);
										}
										else
										poruka = "Potrebno je uneti drzavu";
									}
									else
									{
										poruka = "Ocena treba da bude od 1-5";
									}
								}
								catch(Exception ex){poruka = "ocena mora biti broj";}
						}
						else
							poruka = "telefon je uneti mail";
					}
					else
						poruka = "telefon je uneti telefon";
				}
				else
					poruka = "Potrebno je uneti postanskiBroj";
			}
			else
				poruka = "Potrebno je uneti grad";

		}
		else
			poruka = "Potrebno je uneti naziv";
		
	return poruka;
	}
	
	
	
	
}
