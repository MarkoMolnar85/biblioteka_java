package Kontroleri;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import DAO.Konekcija;
import Entiteti.Citaoc;
import Entiteti.Telefon;
import Entiteti.UzetaKnjiga;
import Entiteti.VracenaKnjiga;
import Entiteti.VracenaKnjiga;

public class Kontroler {
	private String UmanjenjeDoBroja(String textic)
	{
		String skracen="";
		for(int i = 0; i < textic.length(); i++)
		{
			char karakter = textic.charAt(i);
			if(karakter == '-')
				break;
			skracen += karakter;
		}
		return skracen;
	}
	private boolean ProveraDatuma(String datum)
	{
		boolean povratna  = true;
		char karakter = ' ';
		if(datum != null && datum.length() > 0)
		{
			try
			{
				int broj= 0;
				karakter = datum.charAt(0);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(1);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(3);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(4);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(6);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(7);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(8);
				broj = Integer.parseInt("" + karakter);
				karakter = datum.charAt(9);
				broj = Integer.parseInt("" + karakter);
			}
			catch(Exception e)
			{
				povratna = false;
			}
			karakter = datum.charAt(2);
			if(karakter != '/')
				povratna = false;
			karakter = datum.charAt(5);
			if(karakter != '/')
				povratna = false;
		}
		else
			povratna = false;
		
		return povratna;
	}
	public String poruka;
	int brojLicneKarte;
	public String ProveraCitaoca(String brLicne, String ime, String prezime, String rodjenje, String drzava, String grad, String adresa, String pol)
	{
		poruka = "Novi citaoc je unet";
		String imeS;
		String prezimeS;
		String rodjenjeS;
		String drzavaS = drzava;
		String gradS;
		String adresaS;
		String polS;
		try
		{
			this.brojLicneKarte = Integer.parseInt(brLicne);
			imeS = ime;
			if(imeS != null && imeS.length() >0)
			{
				if(prezime != null && prezime.length() >0)
				{
					prezimeS = prezime;
					if(rodjenje !=null && rodjenje.length()> 0 && ProveraDatuma(rodjenje))
					{
						rodjenjeS = rodjenje;
						if(grad !=null && grad.length()> 0)
						{
							gradS = grad;
							if(adresa !=null && adresa.length()> 0)
							{
								adresaS = adresa;
								if(pol !=null && pol.length()> 0 && (pol == "musko" || pol == "zensko"))
								{
									polS = pol;
									Citaoc citaoc = new Citaoc(brojLicneKarte, imeS, prezimeS, polS, rodjenjeS, drzavaS, gradS, adresaS);
									Konekcija konekcija = new Konekcija();
									konekcija.InsertovanjeCitaoca(citaoc);
								}
							}
							else
							{
								poruka = "Potrebno je uneti adresu";
							}
						}
						else
						{
							poruka = "Potrebno je uneti grad";
						}
					}
					else
					{
						poruka = "Potrebno je ispravno uneti datum kao 23/04/2017";
					}
					
				}
				else
				{
					poruka = "Potrebno je uneti prezime";
				}
			}
			else
			{
				poruka = "Potrebno je uneti ime";
			}
		}
		catch(Exception ex)
		{
			poruka = "Pogresno unet broj clanske karte " + ex.getMessage();
		}
		
		return poruka;
	}
	
	public String ProveraTelefona(String brojClanske, String ime, String brojTelefona)
	{
		String poruka = "Novi telefon je unet";
		try
		{
			if(brojClanske !=null && brojClanske.length() > 0)
			{
				if(ime !=null && ime.length() > 0)
				{
					int brojClanskeInt = Integer.parseInt(brojClanske);
					if(brojTelefona !=null && brojTelefona.length() > 0)
					{
						Konekcija kon = new Konekcija();
						int max = kon.MaxIdTelefona();
						++max;
						Telefon tel = new Telefon(max, brojTelefona, brojClanskeInt, ime);
						kon.InsertTelefona(tel);
						
					}
					else
						poruka = "Potrebno je uneti broj telefona";
				}
				else
					poruka = "Potrebno je uneti ime";
			}
			else
				poruka = "Potrebno je uneti broj clanske karte";
			}
		catch(Exception e)
		{
			poruka = "Polja nisu ispravno popunjena";
		}
		
		return poruka;
	}
	
	public String ProveraIzdavanjaKnjige(String datumaUzeto, String brojClanskeKarte, String kataloskiBroj)
	{
		
		String poruka="Knjiga je izdata i evidentirana";
		kataloskiBroj = UmanjenjeDoBroja(kataloskiBroj);
		if(ProveraDatuma(datumaUzeto))
		{
			try{
				int bc = Integer.parseInt(brojClanskeKarte);
				try{
					int kb = Integer.parseInt(kataloskiBroj);
					UzetaKnjiga uzetaKnjiga = new UzetaKnjiga(datumaUzeto, bc, kb);
					Konekcija kon = new Konekcija();
					kon.InsertUzeteKnige(uzetaKnjiga);
					
				}
				catch(Exception e)
				{
					poruka = "Neispravno unet kataloski broj";
				}
				
			}
			catch(Exception e)
			{
				poruka = "Neispravno unet broj clanske karte";
			}
		}
		else
			poruka = "Neispravno unet datum";
		return poruka;
	}
	
	public String ProveraVracanjaKnjige(String datum, String brojClanske, String kataloskiBroj)
	{
		String poruka = "Vracanje knjige je memorisano";
		try{
		
		if(brojClanske !=null)
		brojClanske = UmanjenjeDoBroja(brojClanske);
		if(kataloskiBroj !=null)
		kataloskiBroj = UmanjenjeDoBroja(kataloskiBroj);
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u kontroleru za vracanje knjige");}
		
		if(ProveraDatuma(datum))
		{
			if(brojClanske != null && brojClanske.length() > 0)
			{
				if(kataloskiBroj != null && kataloskiBroj.length() > 0)
				{
					try
					{
						int a = Integer.parseInt(brojClanske);
						try
						{
							int b = Integer.parseInt(kataloskiBroj);
							VracenaKnjiga vracenaKnjiga = new VracenaKnjiga(datum, a, b);
							Konekcija konekcija = new Konekcija();
							konekcija.UpdateVracanjeKnjige(vracenaKnjiga);
							
						}
						catch(Exception e)
						{
							poruka = "Kataloski broj mora da bude broj";
						}
					}
					catch(Exception e)
					{
						poruka = "Broj clanske karte mora da bude broj";
					}
					
				}
			}
			else
				poruka = "Broj clanske karte nije unet";
		}
		else
			poruka = "Datum nije ispravno unet";
		
		return poruka;
	}
	
	
	
	
	
}