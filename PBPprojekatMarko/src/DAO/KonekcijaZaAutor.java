package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;

import Entiteti.Autor;

public class KonekcijaZaAutor {
	
	public int SledeciIdAutora()
	{
		int id = 0;
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select max(id) as max from autori";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			int brojac = 0;
			while(rs.next())
			{
				id = rs.getInt("max");
				id++;
				brojac++;
			}
			if(brojac == 0)
			{
				id = 1;
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji SledeciIdAutora");}
		return id;
	}
	public void InsertAutora(Autor autor)
	{
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into autori(id, ime, prezime, drzava, godina_rodjenja) values (?, ?, ?, ?, to_date('"+ autor.getDatumRodjenja() +"', 'dd/mm/yyyy'))";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, autor.getId());
			ps.setString(2, autor.getIme());
			ps.setString(3, autor.getPrezime());
			ps.setString(4, autor.getDrzava());
			ResultSet rs = ps.executeQuery();
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji insertovanje autora");}
	}
}
