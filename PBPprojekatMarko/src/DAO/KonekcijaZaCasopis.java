package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Entiteti.Casopis;
import Entiteti.Rad;

public class KonekcijaZaCasopis {
	
	public ArrayList<String> BogacenjeKomboBoxaIzdavac()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select id, naziv from izdavaci order by naziv";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String str = rs.getString(1);
				str += "-" + rs.getString(2);
				lista.add(str);
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji Bogacenje KomboBoxa Izdavac");}
		return lista;
	}
	
	public void InsertovanjeCasopisa(Casopis casopis)
	{
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into casopisi (issn, naslov, KATALOSKI_BROJ, urednik, id_izdavac) values (?, ?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, casopis.getIssn());
			ps.setString(2, casopis.getNaslov());
			ps.setInt(3, casopis.getKataloskiBroj());
			ps.setString(4, casopis.getUrednik());
			ps.setInt(5, casopis.getIzdavac());
			ResultSet rs = ps.executeQuery();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji Insertovanje Casopisa");}
	}
	
	public ArrayList<String> BogacenjeKomboBoxaAutoraUradovima()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select id, ime, prezime from autori order by prezime";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String str = rs.getString(1);
				str += "-" + rs.getString(2);
				str += "-" + rs.getString(3);
				lista.add(str);
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji Bogacenje KomboBoxa Autora U radovima");}
		return lista;
	}
	
	public void InsertovanjeRadova(Rad rad)
	{
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into radovi (naslov, issn, id_autor) values (?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, rad.getNaslov());
			ps.setString(2, rad.getIssn());
			ps.setInt(3, rad.getIdAutora());
			ResultSet rs = ps.executeQuery();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji Insertovanje rada");}
	}
	
}













