package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Entiteti.Drzava;
import Entiteti.Izdavac;

public class KonekcijaZaIzdavaca {
	public int SledeciIdDrzave()
	{
		int n = 0;
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select max(id) as max from drzave";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			int brojac = 0;
			while(rs.next())
			{
				n = rs.getInt(1);
				brojac++;
				++n;
			}
			if(brojac == 0)
			{
				n = 1;
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji sledeci id drzave");}
		return n;
	}
	
	public void InserovanjeDrzave(Drzava drzava)
	{
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into drzave (id, drzava) values(?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, drzava.getId());
			ps.setString(2, drzava.getDrzava());
			ResultSet rs = ps.executeQuery();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska u konekciji insertovanje drzave");}
	}
	
	
	public ArrayList<String>ObogacivanjeKomboBoxaIzdavacaDrzava()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select id, drzava from drzave order by id";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String str = rs.getString(1);
				str+= "-" + rs.getString(2);
				lista.add(str);
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska u konekciji obogacivanje kombo boxa drzava");}
		return lista;
	}
	
	public void InsertovanjeIzdavaca(Izdavac izdavac)
	{
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into izdavaci (id, naziv, grad, POSTANSKI_BROJ, TELEFON, mail, ocena, ID_DRZAVA) values (?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, izdavac.getId());
			ps.setString(2, izdavac.getNaziv());
			ps.setString(3, izdavac.getGrad());
			ps.setString(4, izdavac.getPostanskiBroj());
			ps.setString(5, izdavac.getTelefon());
			ps.setString(6, izdavac.getMail());
			ps.setInt(7, izdavac.getOcena());
			ps.setInt(8, izdavac.getIdDrzava());
			ResultSet rs = ps.executeQuery();

		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska u konekciji insertovanje izdavaca");}
	}
	
	public int SledeciIDIzdvaca()
	{
		int n = 0;
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select max(id) as max from izdavaci";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			int brojac = 0;
			while(rs.next())
			{
				n = rs.getInt(1);
				++n;
				brojac++;
			}
			if(brojac == 0)
			{
				n = 1;
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska u konekciji SledeciIDIzdvaca");}
		return n;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
