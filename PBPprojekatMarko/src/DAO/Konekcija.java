package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Entiteti.Citaoc;
import Entiteti.Telefon;
import Entiteti.UzetaKnjiga;
import Entiteti.VracenaKnjiga;

public class Konekcija {
	////////////////////////////////
		public Konekcija()
		{
			try
			{
			/*Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();*/
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Greska pri konekciji");
			}
		}
		/////////////////////////////////
		public void InsertovanjeCitaoca(Citaoc citac)
		{
			//
			try{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    st = con.createStatement();
				String sql = "insert into citaoci (broj_clanske_karte, ime, prezime, pol, godina_rodjenja, drzava, grad, adresa)"
						+ "values(?, ?, ?, ?, to_date('" +citac.getGodinaRodjenja() + "' ,'dd//mm//yyyy') , ?, ?, ?)";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setInt(1, citac.getBrojClanskeKarte());
				ps.setString(2, citac.getIme());
				ps.setString(3, citac.getPrezime());
				ps.setString(4, citac.getPol());
				ps.setString(5, citac.getDrzava());
				ps.setString(6, citac.getGrad());
				ps.setString(7, citac.getGrad());
				ResultSet result = ps.executeQuery();
				con.close();
				//JOptionPane.showMessageDialog(null, "Kraj funkcije u konekciji");
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " eksepsn na konekciji");
			}
			
		}
		public ArrayList<String> SelectPopunjavanjeKomboBoxaCitaoci()
		{
			ArrayList<String>lista = new ArrayList<String>(); 
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "select broj_clanske_karte, ime, prezime from citaoci order by prezime";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ResultSet rs = ps.executeQuery();
			    while(rs.next())
			    {
			    	String str = "";
			    	int a = rs.getInt("broj_clanske_karte");
			    	str = "" + a;
			    	str += "-" + rs.getString("ime");
			    	str += "-" + rs.getString("prezime");
			    	lista.add(str);
			    }
			    con.close();
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
			return lista;
		}
		public Citaoc SELECTspecificniCItaoc(int brClanske)
		{
			Citaoc cit = new Citaoc();
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "select broj_clanske_karte, ime, prezime, pol, godina_rodjenja, drzava, grad, adresa from citaoci where broj_clanske_karte = "+brClanske+"";                          
				PreparedStatement ps = con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					cit.setBrojClanskeKarte(rs.getInt("broj_clanske_karte"));
					cit.setIme(rs.getString("ime"));
					cit.setPrezime(rs.getString("prezime"));
					cit.setPol(rs.getString("pol"));
					cit.setGodinaRodjenja(rs.getString("godina_rodjenja"));
					cit.setDrzava(rs.getString("drzava"));
					cit.setGrad(rs.getString("grad"));
					cit.setAdresa(rs.getString("adresa")); 
					
				}
				con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " greska specifican citaoc");
			}
			return cit;
		}
		public int MaxIdTelefona()
		{
			int max = 0;
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "select max(id) as max from telefoni";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ResultSet rs = ps.executeQuery();
			    
			    while(rs.next())
			    {
			    	max = rs.getInt("max");
			    }
			    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, "Greksa na max telefona konekciji");
			}
			return max;
		}
		public void InsertTelefona(Telefon tel)
		{
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "insert into telefoni (id, broj_Telefona, broj_clanske_karte) values(?, ?, ?)";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ps.setInt(1, tel.getId());
			    ps.setString(2, tel.getBrojTelefona());
			    ps.setInt(3, tel.getBrojClanskeKarte());
			    
			    ResultSet rs = ps.executeQuery();
			    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, " greska na konekciji telefona insert");
			}
		}
		
		public void InsertUzeteKnige(UzetaKnjiga uzetaKnjiga)
		{
			try
			{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    String sql = "insert into izdati_primerci (datuma_uzeto, br_clanske_karte, kataloski_broj) values(to_date('"+ uzetaKnjiga.getDatumauzeto()  +"', 'dd/mm/yyyy'), ?, ?)";
		    PreparedStatement ps = con.prepareStatement(sql);
		    ps.setInt(1, uzetaKnjiga.getBrojClanskeKarte());
		    ps.setInt(2, uzetaKnjiga.getKataloskiBroj());
		    ResultSet rs  = ps.executeQuery();
		    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " greska kod inserta uzete knjige");
			}
		}
		
		public ArrayList<String>PopuluacijaKomboBoxaKataloskiBroj()
		{
			ArrayList<String>lista = new ArrayList<String>();
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "select kataloski_broj, naziv from knjige";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ResultSet rs = ps.executeQuery();
			    while(rs.next())
			    {
			    	String str = "";
			    	int n = rs.getInt("kataloski_broj");
			    	String naziv = rs.getString("naziv");
			    	str = n + "-" + naziv;
			    	lista.add(str);
			    }
			    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " greska kod poulacije kombo boxa ya katalog");
			}
			
			return lista;
		}
		public ArrayList<String>PopuluacijaKomboBoxaKataloskiBrojZaVracanjeKnjige()
		{
			ArrayList<String>lista = new ArrayList<String>();
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "select izdati_primerci.kataloski_broj, knjige.naziv from knjige join izdati_primerci on knjige.kataloski_broj = izdati_primerci.kataloski_broj where izdati_primerci.datuma_vraceno is null";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ResultSet rs = ps.executeQuery();
			    while(rs.next())
			    {
			    	String str = "";
			    	int n = rs.getInt("kataloski_broj");
			    	String naziv = rs.getString("naziv");
			    	str = n + "-" + naziv;
			    	lista.add(str);
			    }
			    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " greska kod populacije kombo boxa za kataloski broj");
			}
			
			return lista;
		}
		
		public void UpdateVracanjeKnjige(VracenaKnjiga vracenaKnjiga)
		{
			try{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    
			    String sql = "update izdati_primerci set DATUMA_VRACENO = to_date('"+ vracenaKnjiga.getDatumaVraceno() +"', 'dd/mm/yyyy') where BR_CLANSKE_KARTE = ? and KATALOSKI_BROJ = ?";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ps.setInt(1, vracenaKnjiga.getBrojClanskeKarte());
			    ps.setInt(2, vracenaKnjiga.getKataloskiBroj());
			    ResultSet rs = ps.executeQuery();
			    //ps.executeUpdate(); //za razliku od ostalih qverija vde ide execute update
			    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " Greska je kod update vracanje knjige");	
			}
		    
		}
		
		public ArrayList<String>PopuluacijaKomboBoxaKataloskiBrojZaIzdavanjeKnjige()
		{
			ArrayList<String>lista = new ArrayList<String>();
			try
			{
				Connection con;
				Statement st;
				Class.forName("oracle.jdbc.driver.OracleDriver");
			    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
			    String sql = "select distinct(knjige.kataloski_broj), naziv from knjige left join izdati_primerci on knjige.kataloski_broj = izdati_primerci.kataloski_broj where knjige.kataloski_broj not in (select kataloski_broj from izdati_primerci where datuma_vraceno is null)";
			    PreparedStatement ps = con.prepareStatement(sql);
			    ResultSet rs = ps.executeQuery();
			    while(rs.next())
			    {
			    	String str = "";
			    	int n = rs.getInt("kataloski_broj");
			    	String naziv = rs.getString("naziv");
			    	str = n + "-" + naziv;
			    	lista.add(str);
			    }
			    con.close();
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, ex.getMessage() + " greska kod populacije kombo boxa za kataloski broj");
			}
			
			return lista;
		}
		
		
		
		
		
		
		
		
}