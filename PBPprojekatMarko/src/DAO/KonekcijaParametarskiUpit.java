package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Entiteti.BrojKnjigaZaZanr;
import Entiteti.Citaoc;
import Entiteti.Knjiga;
import Entiteti.Rad;

public class KonekcijaParametarskiUpit {

	public ArrayList<Citaoc> CitaociNaOsnovuImenaIliPrezimenaSelect(String ime, String prezime)
	{
		ArrayList<Citaoc>lista = new ArrayList<Citaoc>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from citaoci where lower(ime) like ? or lower(prezime) like ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, ime + "%");
			ps.setString(2, prezime + "%");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Citaoc citaoc = new Citaoc();
				citaoc.setBrojClanskeKarte(rs.getInt(1));
				citaoc.setIme(rs.getString(2));
				citaoc.setPrezime(rs.getString(3));
				citaoc.setPol(rs.getString(4));
				citaoc.setGodinaRodjenja(rs.getString(5));
				citaoc.setDrzava(rs.getString(6));
				citaoc.setGrad(rs.getString(7));
				citaoc.setAdresa(rs.getString(8));
				lista.add(citaoc);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji KonekcijaParametarskiUpit");}
		return lista;
	}
	
	public ArrayList<Knjiga> KnjigaPoBiloKomOsnovu(String str)
	{
		ArrayList<Knjiga>lista = new ArrayList<Knjiga>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from knjige where kataloski_broj like ? or lower(naziv) like ? or ostecenje like ? or mesto like ? or naziv_zanra like ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, str + "%");
			ps.setString(2, str + "%");
			ps.setString(3, str + "%");
			ps.setString(4, str + "%");
			ps.setString(5, str + "%");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Knjiga knjiga = new Knjiga();
				knjiga.setKataloskiBroj(rs.getInt(1));
				knjiga.setNaziv(rs.getString(2));
				knjiga.setOstecenje(rs.getString(3));
				knjiga.setMesto(rs.getString(4));
				knjiga.setNazivZanra(rs.getString("naziv_zanra"));
				lista.add(knjiga);
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji KnjigaPoBiloKomOsnovu");}
		return lista;
	}
	
	public ArrayList<Rad> RadPoBiloKomOsnovu(String str)
	{
		ArrayList<Rad>lista = new ArrayList<Rad>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from radovi where lower(naslov) like ? or issn like ? order by naslov";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, str + "%");
			ps.setString(2, str + "%");
	
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Rad r = new Rad();
				r.setNaslov(rs.getString(1));
				r.setIssn(rs.getString(2));
				lista.add(r);
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji KnjigaPoBiloKomOsnovu");}
		return lista;
	}
}
