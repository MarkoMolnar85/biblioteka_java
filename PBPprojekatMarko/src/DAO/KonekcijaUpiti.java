package DAO;

import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Entiteti.BrojKnjigaZaZanr;
import Entiteti.Citaoc;
import Entiteti.DrzavaIzdavacCasopisRad;
import Entiteti.IzdanjeSaAutorom;
import Entiteti.IzdatPrimerak;
import Entiteti.Knjiga;
import Entiteti.Telefon;

public class KonekcijaUpiti {
	
	public ArrayList<Citaoc> PregledCitaoca()
	{
		ArrayList<Citaoc>listaCitaoca = new ArrayList<Citaoc>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from citaoci order by prezime";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
	
			while(rs.next())
			{
				Citaoc cit = new Citaoc();
				cit.setBrojClanskeKarte(rs.getInt(1));
				cit.setIme(rs.getString(2));
				cit.setPrezime(rs.getString(3));
				cit.setPol(rs.getString(4));
				cit.setGodinaRodjenja(rs.getString(5));
				cit.setDrzava(rs.getString(6));
				cit.setGrad(rs.getString(7));
				cit.setAdresa(rs.getString(8));
				listaCitaoca.add(cit);
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji PregledCitaoca");}
		return listaCitaoca;
	}
	public ArrayList<Telefon> PregledTelefona()
	{
		ArrayList<Telefon>lista = new ArrayList<Telefon>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from telefoni order by ime";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Telefon tel = new Telefon();
				tel.setId(rs.getInt(1));
				tel.setBrojTelefona(rs.getString(2));
				tel.setBrojClanskeKarte(rs.getInt(3));
				tel.setIme(rs.getString(4));
				lista.add(tel);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji PregledCitaoca");}
		return lista;
	}
	
	public ArrayList<IzdatPrimerak> IzdateKnjige()
	{
		ArrayList<IzdatPrimerak>lista = new ArrayList<IzdatPrimerak>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from izdati_primerci order by datuma_uzeto desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				IzdatPrimerak izdatPrimerak = new IzdatPrimerak();
				izdatPrimerak.setDatumaUzeto(rs.getString(1));
				izdatPrimerak.setDatumaVraceno(rs.getString(2));
				izdatPrimerak.setBrojClanskeKarte(rs.getInt(3));
				izdatPrimerak.setKataloskiBroj(rs.getInt(4));
				lista.add(izdatPrimerak);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji IzdateKnjige");}
		return lista;
	}
	
	public ArrayList<Knjiga> Knjige()
	{
		ArrayList<Knjiga>lista = new ArrayList<Knjiga>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige order by naziv";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Knjiga k = new Knjiga();
				k.setKataloskiBroj(rs.getInt(1));
				k.setNaziv(rs.getString(2));
				k.setOstecenje(rs.getString(3));
				k.setMesto(rs.getString(4));
				k.setNazivZanra(rs.getString(5));
				lista.add(k);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji knjige");}
		return lista;
	}
	
	public ArrayList<IzdanjeSaAutorom> IzdanjaAutori()
	{
		ArrayList<IzdanjeSaAutorom>lista = new ArrayList<IzdanjeSaAutorom>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select izdanja.naziv_knjige, izdanja.isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				IzdanjeSaAutorom izdanjeAutor = new IzdanjeSaAutorom();
				izdanjeAutor.setNazivKnjige(rs.getString(1));
				izdanjeAutor.setIsbn(rs.getString(2));
				izdanjeAutor.setAutorKoautor(rs.getString(3));
				izdanjeAutor.setPrezime(rs.getString(4));
				lista.add(izdanjeAutor);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji IzdanjaAutori");}
		return lista;
	}
	
	public ArrayList<DrzavaIzdavacCasopisRad> IzdavacCaspoisRad()
	{
		ArrayList<DrzavaIzdavacCasopisRad>lista = new ArrayList<DrzavaIzdavacCasopisRad>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select d.drzava, i.naziv, c.issn, c.kataloski_broj, c.urednik, r.naslov from drzave d join izdavaci i on d.id = i.id_drzava join casopisi c on i.id = c.id_izdavac join radovi r on c.issn = r.issn";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DrzavaIzdavacCasopisRad dicr = new DrzavaIzdavacCasopisRad();
				dicr.setDrzava(rs.getString(1));
				dicr.setIssn(rs.getString(3));
				dicr.setNaziv(rs.getString(2));
				dicr.setKataloskiBroj(rs.getInt(4));
				dicr.setUrednik(rs.getString(5));
				dicr.setNaslov(rs.getString(6));
				lista.add(dicr);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji CasopisiRadovi");}
		return lista;
	}
	
	public ArrayList<BrojKnjigaZaZanr> ZanroviBrojKnjigaSelect()
	{
		ArrayList<BrojKnjigaZaZanr>lista = new ArrayList<BrojKnjigaZaZanr>();
		try
		{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select upper(zanrovi.naziv_zanra), count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv) desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				BrojKnjigaZaZanr bkpz = new BrojKnjigaZaZanr();
				bkpz.setNazivZanra(rs.getString(1));
				bkpz.setBrojKnjiga(rs.getInt(2));
				
				lista.add(bkpz);
				
			}
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji CasopisiRadovi");}
		return lista;
	}
}
