package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Entiteti.AutorKnjige;
import Entiteti.Izdanje;
import Entiteti.Knjiga;
import Entiteti.Zanr;

public class KonekcijaZaKnjige {
	public int MaxIDZanra()
	{
		int id = 0;
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select max(id) as max from zanrovi";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				id = rs.getInt("max");
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, "Greska u konekciji maxIdZanra");}
		return id;
	}
	
	public void InsertovanejZanra(Zanr zanr)
	{
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into zanrovi (id, naziv_zanra) values(?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, zanr.getId());
			ps.setString(2, zanr.getZanr());
			ResultSet rs = ps.executeQuery();
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " Greska u konekciji insertovanje zanra");}
	}
	
	public ArrayList<String> ObogoacivanjeKomboBoxaZanrovi()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select naziv_zanra from zanrovi order by naziv_zanra";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				lista.add(rs.getString(1));
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " Greska u konekciji obogacivanje komboxoxa zanrovi");}
		return lista;
	}
	public ArrayList<String>ObogacivanjeKomboBoxaKataloskiBrojeviKnjiga(String zanr)
	{
		ArrayList<String>lista = new ArrayList<String>();
		
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select knjige.KATALOSKI_BROJ, knjige.NAZIV from zanrovi join knjige on zanrovi.id = knjige.id_zanra where zanrovi.naziv_zanra = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, zanr);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String str = "";
				str = "" + rs.getInt("kataloski_broj");
				str += "-"; 
				str += rs.getString("naziv");
				lista.add(str);
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " Greska u konekciji obogacivanje komboxoxa kataloski brojevi knjiga");}
		return lista;
	}
	
	public Knjiga SelectKnjigeNaOsnovuNazivaZanra(int kataloskiBro)
	{
		Knjiga knjiga = null;
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select * from knjige where KATALOSKI_BROJ = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, kataloskiBro);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				int intKataloski = rs.getInt(1);
				String strnaziv = rs.getString(2);
				String strMesto = rs.getString(4);
				knjiga = new Knjiga();
				knjiga.setKataloskiBroj(intKataloski);
				knjiga.setNaziv(strnaziv);
				knjiga.setMesto(strMesto);
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " Greska u konekciji selekt knjige na osnovu zanra");}
		
		
		return knjiga;
	}
	
	public int VracaIDzanraNaOsnovuNazivaZanra(String nazivZanra)
	{
		int n = 0;
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select id from zanrovi where naziv_zanra = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, nazivZanra);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				n = rs.getInt(1);
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji vracanje id na osnovu naziva zanra");}
		return n;
	}
	
	public void InsertovanjeKnjige(Knjiga knjiga)
	{
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into knjige (kataloski_broj, naziv, ostecenje, mesto, id_zanra) values (?, ?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, knjiga.getKataloskiBroj());
			ps.setString(2, knjiga.getNaziv());
			ps.setString(3, knjiga.getOstecenje());
			ps.setString(4, knjiga.getMesto());
			ps.setInt(5, knjiga.getIdZanra());
			ResultSet rs = ps.executeQuery();
			
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji vracanje id na osnovu naziva zanra");}
	}
	
	public ArrayList<String>BogacenjeKomboBoxaIzborIzdavacaKodIzdanja()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select id, naziv from izdavaci";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String str = "";
				str += rs.getInt(1);
				str += "-" +  rs.getString(2);
				lista.add(str);
			}
			
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji bogacenje komboBoxaIzdavaci u izdanju");}
		return lista;
	}
	
	public void InsertovanjeIzdanja(Izdanje izdanje)
	{
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into izdanja (datum, isbn, BROJ_STRANA, cena, br_izdanja, vez, KATALOSKI_BROJ, ID_IZDAVAC) values (to_date('"+ izdanje.getDatum() +"', 'dd,mm,yyyy'), ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, izdanje.getIsbn());
			ps.setInt(2, izdanje.getBrojStrana());
			ps.setDouble(3, izdanje.getCena());
			ps.setInt(4, izdanje.getBrojIzdanja());
			ps.setString(5, izdanje.getVez());
			ps.setInt(6, izdanje.getKataloskiBroj());
			ps.setInt(7, izdanje.getIzdavac());
			ResultSet rs = ps.executeQuery();
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji insertovanje izdanja");}
	}
	
	public ArrayList<String> BogacenjeKomboBoxaAutoriISBN()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select isbn from izdanja";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				lista.add(rs.getString(1));
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji kod bogacenja komboboxa ISBN");}
		return lista;
	}
	
	public ArrayList<String> BogacenjeKomboBoxaAutorImenaAutora()
	{
		ArrayList<String>lista = new ArrayList<String>();
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "select id, ime, prezime from autori";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				String str = rs.getString(1);
				str += "-" + rs.getString(2);
				str += "-" + rs.getString(3);
				lista.add(str);
			}
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji kod bogacenja komboboxa imena autora");}
		return lista;
	}
	
	public void InsertAutoraKnjige(AutorKnjige autor)
	{
		try{
			Connection con;
			Statement st;
			Class.forName("oracle.jdbc.driver.OracleDriver");
		    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","projekatsema","projekatsifraseme");
		    st = con.createStatement();
			String sql = "insert into autori_knjiga (AUTOR_KOAUTOR, ISBN_IZDANJE, ID_AUTOR) values (?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, autor.getAutorKoautor());
			ps.setString(2, autor.getIsbn());
			ps.setInt(3, autor.getId());
			
			ResultSet rs = ps.executeQuery();
			con.close();
		}
		catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage() + " greska je u konekciji kod insertovanja autora knjige");}
	}
	
	
}
