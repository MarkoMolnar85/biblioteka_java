set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_default_workspace_id=>5390342145294642
);
end;
/
prompt  WORKSPACE 5390342145294642
--
-- Workspace, User Group, User, and Team Development Export:
--   Date and Time:   14:08 Wednesday September 13, 2017
--   Exported By:     ADMIN
--   Export Type:     Workspace Export
--   Version:         5.1.2.00.09
--   Instance ID:     211610639210671
--
-- Import:
--   Using Instance Administration / Manage Workspaces
--   or
--   Using SQL*Plus as the Oracle user APEX_050100
 
begin
    wwv_flow_api.set_security_group_id(p_security_group_id=>5390342145294642);
end;
/
----------------
-- W O R K S P A C E
-- Creating a workspace will not create database schemas or objects.
-- This API creates only the meta data for this APEX workspace
prompt  Creating workspace WORKSPACEPROJEKAT...
begin
wwv_flow_fnd_user_api.create_company (
  p_id => 5390525514294854
 ,p_provisioning_company_id => 5390342145294642
 ,p_short_name => 'WORKSPACEPROJEKAT'
 ,p_display_name => 'WORKSPACEPROJEKAT'
 ,p_first_schema_provisioned => 'PROJEKATSEMA'
 ,p_company_schemas => 'PROJEKATSEMA'
 ,p_ws_schema => 'PROJEKATSEMA'
 ,p_account_status => 'ASSIGNED'
 ,p_allow_plsql_editing => 'Y'
 ,p_allow_app_building_yn => 'Y'
 ,p_allow_packaged_app_ins_yn => 'Y'
 ,p_allow_sql_workshop_yn => 'Y'
 ,p_allow_websheet_dev_yn => 'Y'
 ,p_allow_team_development_yn => 'Y'
 ,p_allow_to_be_purged_yn => 'Y'
 ,p_allow_restful_services_yn => 'Y'
 ,p_source_identifier => 'WORKSPAC'
 ,p_path_prefix => 'WORKSPACEPROJEKAT'
 ,p_files_version => 1
);
end;
/
----------------
-- G R O U P S
--
prompt  Creating Groups...
prompt  Creating group grants...
----------------
-- U S E R S
-- User repository for use with APEX cookie-based authentication.
--
prompt  Creating Users...
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id                      => '5390242580294642',
  p_user_name                    => 'ADMIN',
  p_first_name                   => 'marko',
  p_last_name                    => 'molnar',
  p_description                  => '',
  p_email_address                => 'marko.molnar855@gmail.com',
  p_web_password                 => 'E61E17C0E50077E2C6E8431FB308BA2771263896',
  p_web_password_format          => '5;2;10000',
  p_group_ids                    => '',
  p_developer_privs              => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
  p_default_schema               => 'PROJEKATSEMA',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('201709022102','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'Y',
  p_first_password_use_occurred  => 'Y',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
----------------
--App Builder Preferences
--
----------------
--Click Count Logs
--
----------------
--csv data loading
--
----------------
--mail
--
----------------
--mail log
--
----------------
--app models
--
----------------
--password history
--
begin
  wwv_flow_api.create_password_history (
    p_id => 5390631373294883,
    p_user_id => 5390242580294642,
    p_password => '63BB48553479C2C8C1ED8DF839481F9B4F6CECDF');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 5408856333322537,
    p_user_id => 5390242580294642,
    p_password => 'E61E17C0E50077E2C6E8431FB308BA2771263896');
end;
/
----------------
--preferences
--
begin
  wwv_flow_api.create_preferences$ (
    p_id => 5465468478829160,
    p_user_id => 'ADMIN',
    p_preference_name => 'F4500_1157686386582338224_SPLITTER_STATE',
    p_attribute_value => '274:false');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 5580256026048465,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P1500_W3519715528105919',
    p_attribute_value => '3521529006112497____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 5580580811055507,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4350_P25_W9307709001462905',
    p_attribute_value => '9309908769463151____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 5580872596055594,
    p_user_id => 'ADMIN',
    p_preference_name => 'PERSISTENT_ITEM_P25_DATE',
    p_attribute_value => '.125');
end;
/
----------------
--query builder
--
----------------
--sql scripts
--
----------------
--sql commands
--
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5552948039000967
 ,p_command => 
'delete from drzave where dzrva = ''Peru'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092223','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5553056582002803
 ,p_command => 
'delete from drzave where drzava= ''Peru'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092224','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5554478509195911
 ,p_command => 
'insert into izdavaci (id, naziv, grad, POSTANSKI_BROJ, TELEFON, mail, ocena, ID_DRZAVA) values (15, ''Vulkan'', ''Beoograd'', ''11000'', ''01145645643'', ''vulkan@gmail.com'', 4, 1)'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092220','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5554505516196477
 ,p_command => 
'select * from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092220','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5554699493216874
 ,p_command => 
'delete from izdavaci where id = 15'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092224','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5554782770218794
 ,p_command => 
'select max(id) as max from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092224','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5554870423324601
 ,p_command => 
'select * from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092317','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5554969636426017
 ,p_command => 
'delete from izdavaci where id = 16'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709092334','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555061702699452
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100020','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555139463701170
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100020','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555268022712652
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100022','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555394203712935
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100022','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555495401715861
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100022','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555523954720988
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100023','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555630308727155
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100024','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555717129728750
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100025','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555810072732801
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100025','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5555993057797578
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100036','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556089442815894
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100013','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556155599841086
 ,p_command => 
'select * from izdavac'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100017','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556232111841702
 ,p_command => 
'select * from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100017','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556364960843491
 ,p_command => 
'select id, naziv from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100017','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556476470948326
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100035','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556570268964477
 ,p_command => 
'select id, naziv from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556616144967523
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556763193967890
 ,p_command => 
'insert into casopisi (issn, naslov, KATALOSKI_BROJ, urednik, id_izdavac) values (0354-3142, ''scIndex'', 6, ''Mislo Mitrovic'', 14)'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556879187970526
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5556983277976036
 ,p_command => 
'delete from casopisi where issn = -2788'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557035590976763
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100040','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557131928980217
 ,p_command => 
'delete from casopisi where kataloski_broj = 6'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100040','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557271848032783
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100049','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557338826036844
 ,p_command => 
'delete from casopisi where kataloski_broj = 7'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100050','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557407784061764
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100054','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557523864107717
 ,p_command => 
'select * from autori'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100101','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557655076109942
 ,p_command => 
'select id, ime, prezime from autori'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100102','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557771483111683
 ,p_command => 
'select id, ime, prezime from autori order by prezime'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100102','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557809022182643
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100014','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5557921635251316
 ,p_command => 
'insert into radovi (naslov, issn, id_autor) values (''abv'', ''3232ffds'', 11)'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100152','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558002854252592
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100152','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558120234253792
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100152','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558231894255798
 ,p_command => 
'insert into radovi (naslov, issn, id_autor) values (''abv'', ''0354-9542'', 11)'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100152','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558341848258421
 ,p_command => 
'delete from radovi where naslov = ''abv'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100153','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558459572279567
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100156','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558549175281872
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100157','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558628931287336
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100158','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558708249303108
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100200','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558807181313211
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100202','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5558910908326529
 ,p_command => 
'delete from radovi where naslov = ''abv'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709100204','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560122403562207
 ,p_command => 
'select * from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101818','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560216488198626
 ,p_command => 
'update citaoci set pol = ''zensko'' where prezime = ''kotromanka'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101825','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560324087200967
 ,p_command => 
'update citaoci set pol = ''zensko'' where prezime = ''Kotromanka'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101825','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560451113210951
 ,p_command => 
'update citaoci set pol = ''zensko'' where prezime = ''Milekic'' or prezime = ''Milentijevic'' or prezime = ''Niklic'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101827','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560540654222984
 ,p_command => 
'select * from telefon'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101829','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560635731223864
 ,p_command => 
'select * from telefoni'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101829','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560799763306941
 ,p_command => 
'select * fromizdati primerci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101843','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560877158308243
 ,p_command => 
'select * from izdati_primerci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101843','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5560968565311598
 ,p_command => 
'select * from izdati_primerci order by datuma_uzeto desc'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101843','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561053340700577
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101848','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561127284709859
 ,p_command => 
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101850','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561278250713014
 ,p_command => 
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige group by mesto'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101850','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561360796713937
 ,p_command => 
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101851','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561449752718563
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101851','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561506372719621
 ,p_command => 
'count distinct naziv group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101851','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561638393721058
 ,p_command => 
'count distinct(naziv) group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101852','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561732723724394
 ,p_command => 
'count distinct(naziv) from knjige group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101852','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561826387727946
 ,p_command => 
'update citaoci set pol = ''zensko'' where prezime = ''Milekic'' or prezime = ''Milentijevic'' or prezime = ''Niklic'''||wwv_flow.LF||
'select * from telefoni'||wwv_flow.LF||
''||wwv_flow.LF||
'select * from izdati_primerci order by datuma_uzeto desc'||wwv_flow.LF||
''||wwv_flow.LF||
'select * from knjige'||wwv_flow.LF||
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige'||wwv_flow.LF||
''||wwv_flow.LF||
'select count( distinct(naziv)) from knjige group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101853','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5561943817746476
 ,p_command => 
'select count(naziv) from knjige group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101856','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562031942748394
 ,p_command => 
'select count(distinct(naziv)) from knjige group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101856','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562136360754605
 ,p_command => 
'select * from zanr'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101857','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562237275755120
 ,p_command => 
'select * from zanrovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101857','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562347286760223
 ,p_command => 
'select naziv_zanra, count(distinct(naziv)) from knjige join zanrovi group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101858','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562460799761668
 ,p_command => 
'select * from zanrovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101858','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562511422762237
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101859','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562675780763591
 ,p_command => 
'select naziv_zanra, count(distinct(naziv)) from knjige join zanrovi on id = id_zanra group by id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101859','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562701060768165
 ,p_command => 
'select naziv_zanra, count(distinct(naziv)) from knjige join zanrovi on zanrovi.id = knjige.id_zanra group by knjige.id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101900','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562883466779691
 ,p_command => 
'select naziv_zanra, count(distinct(naziv)) from knjige join zanrovi on zanrovi.id = knjige.id_zanra '
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101902','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5562934900781829
 ,p_command => 
'select naziv_zanra, from knjige join zanrovi on zanrovi.id = knjige.id_zanra '
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101902','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563036597784011
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101902','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563178523785210
 ,p_command => 
'select naziv_zanra, from knjige join zanrovi on zanrovi.id = knjige.ID_ZANRA'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101902','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563284803786113
 ,p_command => 
'select * from zanrovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101903','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563341676788632
 ,p_command => 
'select naziv_zanra, from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101903','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563460139790029
 ,p_command => 
'select naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101903','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563593311793264
 ,p_command => 
'select zanrovi.naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101904','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563659766796130
 ,p_command => 
'select zanrovi.naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA oreder by knjige.id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101904','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563703265797704
 ,p_command => 
'select zanrovi.naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA order by knjige.id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101905','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563879070800850
 ,p_command => 
'select zanrovi.naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by knjige.id_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101805','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5563928932803056
 ,p_command => 
'select zanrovi.naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101805','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564077017804188
 ,p_command => 
'select * from zanrovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101806','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564129574805172
 ,p_command => 
'select zanrovi.naziv_zanra from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101806','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564262426807448
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjge.naslov) from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101806','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564391044809542
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naslov) from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101806','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564404730810103
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101807','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564581074811397
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101807','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564642915814664
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra oredr by knjige.naziv'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101807','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564758462816453
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by knjige.naziv'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101808','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564831314819652
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by knjige.broj'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101808','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5564976879820951
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra '
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101808','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565058211823511
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv)'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101809','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565158661824603
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv) desc'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101809','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565251754831941
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101810','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565335850833415
 ,p_command => 
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101810','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565428796867244
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101816','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565505262897407
 ,p_command => 
'select * from izdanja'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101821','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565607516901447
 ,p_command => 
'select * from autori_knjiga'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101822','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565767968907797
 ,p_command => 
'select izdanja.isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja, autori, autori_knjiga'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101823','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565865095914040
 ,p_command => 
'select * from izdanja'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101824','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5565958438915144
 ,p_command => 
'select * from autori_knjiga'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101824','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566024230915927
 ,p_command => 
'select * from izdanja'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101824','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566197897918451
 ,p_command => 
'select * from autori_knjiga'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101825','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566243346931346
 ,p_command => 
'select * from autori'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101827','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566361780933005
 ,p_command => 
'select izdanja.isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori on izdanja.isbn = autori_kniga.isbn_izdanje join autori on autori_kniga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101827','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566493841936008
 ,p_command => 
'select  autori_knjiga.autor_koautor, autori.prezime from izdanja join autori on izdanja.isbn = autori_kniga.isbn_izdanje join autori on autori_kniga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101828','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566533041940815
 ,p_command => 
'select autori_knjiga.autor_koautor, autori.prezime from izdanja join autori on izdanja.isbn = autori_knjiga.isbn_izdanje join autori on autori_knjiga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101828','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566697556942291
 ,p_command => 
'select * from autori_knjiga'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101829','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566754326944409
 ,p_command => 
'select autori_knjiga.autor_koautor, autori.prezime from izdanja join autori on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101829','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566803909948405
 ,p_command => 
'select autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101830','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5566910312949975
 ,p_command => 
'select * from izdanja'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101830','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567043585953797
 ,p_command => 
'select izdanja.naziv_knjige, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101831','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567164461958164
 ,p_command => 
'select izdanja.naziv_knjige, izdanja_isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101831','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567261003959544
 ,p_command => 
'select izdanja.naziv_knjige, izdanja.isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101831','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567313874158605
 ,p_command => 
'select * from drzave'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101905','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567490375159212
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101905','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567503087160884
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101805','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567690289166593
 ,p_command => 
'select * from drzave'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101806','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567768115168179
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101806','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567836365173252
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101807','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5567961757174549
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101807','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568085840178307
 ,p_command => 
'select * from drzave'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101808','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568195983180852
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101808','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568257580185729
 ,p_command => 
'select * from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101809','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568355105190242
 ,p_command => 
'select * from drzave'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101810','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568464471191360
 ,p_command => 
'select * from izdavaci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101810','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568552115196854
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101811','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568637394201765
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101812','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568726580202866
 ,p_command => 
'select d.drzava, i.naziv, c.issn, c.kataloski_broj, c.urednik, r.naslov from drzave d join izdavaci i on d.id = i.id_drzava join casopisi c on i.id = c.id_izdavac join radovi r on c.issn = r.issn'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101812','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568857915298625
 ,p_command => 
'select zanrovi.naziv_zanra, count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv) desc'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101828','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5568958657356390
 ,p_command => 
'select UPPER(zanrovi.naziv_zanra), count(knjige.naziv) as broj from zanrovi join knjige on zanrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv) desc'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101838','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569031496410078
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101847','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569135844445053
 ,p_command => 
'select * from autori_knjiga'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709101852','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569238121566035
 ,p_command => 
'select * from citaoci where ime like ''M'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102348','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569333831566459
 ,p_command => 
'select * from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102348','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569401336567660
 ,p_command => 
'select * from citaoci where ime like ''Milos'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102349','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569521493575252
 ,p_command => 
'select * from citaoci where ime like ''M%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102350','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569682279576405
 ,p_command => 
'update citaoci set pol = ''zensko'' where prezime = ''Milekic'' or prezime = ''Milentijevic'' or prezime = ''Niklic'''||wwv_flow.LF||
'select * from telefoni'||wwv_flow.LF||
''||wwv_flow.LF||
'select * from izdati_primerci order by datuma_uzeto desc'||wwv_flow.LF||
'select * from zanrovi'||wwv_flow.LF||
'select * from knjige'||wwv_flow.LF||
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige'||wwv_flow.LF||
''||wwv_flow.LF||
'select UPPER(zanrovi.naziv_zanra), count(knjige.naziv) as broj from zanrovi join knjige on za'||
'nrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv) desc'||wwv_flow.LF||
'select * from izdanja'||wwv_flow.LF||
'select * from knjige'||wwv_flow.LF||
'select * from autori_knjiga'||wwv_flow.LF||
'select * from autori'||wwv_flow.LF||
'select izdanja.naziv_knjige, izdanja.isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'||wwv_flow.LF||
''||wwv_flow.LF||
'sel'||
'ect * from drzave'||wwv_flow.LF||
'select * from izdavaci'||wwv_flow.LF||
'select * from casopisi'||wwv_flow.LF||
'select * from radovi'||wwv_flow.LF||
''||wwv_flow.LF||
'select d.drzava, i.naziv, c.issn, c.kataloski_broj, c.urednik, r.naslov from drzave d join izdavaci i on d.id = i.id_drzava join casopisi c on i.id = c.id_izdavac join radovi r on c.issn = r.issn'||wwv_flow.LF||
''||wwv_flow.LF||
'select * from citaoci'||wwv_flow.LF||
'select * from citaoci where ime like ''Ma%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102350','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569744557577250
 ,p_command => 
'select * from citaoci where ime like ''M%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102350','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569863569579800
 ,p_command => 
'select * from citaoci where ime like ''Ma%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102351','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5569922374586806
 ,p_command => 
'select * from citaoci where ime lower(like ''Ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102352','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570096553587911
 ,p_command => 
'select * from citaoci where ime UPPER(like ''Ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102352','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570190565591280
 ,p_command => 
'select * from citaoci where upper(ime like ''Ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102353','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570274765592804
 ,p_command => 
'select * from citaoci where ime like ''Ma%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102353','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570355778605908
 ,p_command => 
'select * from citaoci where ime = Upper(''marko'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102355','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570436211608531
 ,p_command => 
'select * from citaoci where ime like upper(''Ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102355','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570568966612232
 ,p_command => 
'select * from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102356','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570613991620521
 ,p_command => 
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102357','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570759092629023
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where ime like lower(''ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102359','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570807623629805
 ,p_command => 
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102359','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5570935710632207
 ,p_command => 
'select * from mali_citaoc where ime like lower(''ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709102359','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571038859634763
 ,p_command => 
'select * from citaoc where ime like lower(''ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110000','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571179091636877
 ,p_command => 
'select * from citaoci where ime like lower(''ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110000','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571222714637974
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where ime like lower(''ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110000','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571373570652126
 ,p_command => 
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110003','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571448562655101
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where ime like lower(''ma%'')'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110003','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571591267656379
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc '
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110003','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571623642659573
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where mali_citaoc like ''m%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110004','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571762492660925
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where mali_citaoc.ime like ''m%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110004','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571810081664361
 ,p_command => 
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjenja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where lower(mali_citaoc.ime) like ''m%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110005','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5571988407666904
 ,p_command => 
'select * from citaoci'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110005','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572081262670952
 ,p_command => 
'select * from citaoci where lower(ime) = ''marko'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110006','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572140761673595
 ,p_command => 
'select * from citaoci where lower(ime) like ''m%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110006','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572212738675940
 ,p_command => 
'select * from citaoci where lower(ime) like ''m%'' or lower(prezime) like ''m'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110007','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572354971677130
 ,p_command => 
'select * from citaoci where lower(ime) like ''m%'' or lower(prezime) like ''m%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110007','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572409024678549
 ,p_command => 
'select * from citaoci where lower(ime) like ''m%'' or lower(prezime) like ''mol%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110007','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572510572680443
 ,p_command => 
'select * from citaoci where lower(ime) like ''mol%'' or lower(prezime) like ''mol%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110007','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572688405827441
 ,p_command => 
'select * from knjiga where isbn like ''23%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110036','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572700996829671
 ,p_command => 
'select * from knjiga where isbn like ''ISBN%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110036','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572861433830703
 ,p_command => 
'select * from knjige where isbn like ''ISBN%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110036','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5572937946832195
 ,p_command => 
'select * from knjige '
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110036','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573070806836158
 ,p_command => 
'select * from izdanja'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110037','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573165107839085
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573263182841360
 ,p_command => 
'select * from knjige where isbn like ''ISBN%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573310937842051
 ,p_command => 
'select * from knjige where naslov like ''ISBN%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573450959842931
 ,p_command => 
'update citaoci set pol = ''zensko'' where prezime = ''Milekic'' or prezime = ''Milentijevic'' or prezime = ''Niklic'''||wwv_flow.LF||
'select * from telefoni'||wwv_flow.LF||
''||wwv_flow.LF||
'select * from izdati_primerci order by datuma_uzeto desc'||wwv_flow.LF||
'select * from zanrovi'||wwv_flow.LF||
'select * from knjige'||wwv_flow.LF||
'select KATALOSKI_BROJ, NAZIV, OSTECENJE, MESTO, NAZIV_ZANRA from knjige'||wwv_flow.LF||
''||wwv_flow.LF||
'select UPPER(zanrovi.naziv_zanra), count(knjige.naziv) as broj from zanrovi join knjige on za'||
'nrovi.id = knjige.ID_ZANRA group by zanrovi.naziv_zanra order by count(knjige.naziv) desc'||wwv_flow.LF||
'select * from izdanja'||wwv_flow.LF||
'select * from knjige'||wwv_flow.LF||
'select * from autori_knjiga'||wwv_flow.LF||
'select * from autori'||wwv_flow.LF||
'select izdanja.naziv_knjige, izdanja.isbn, autori_knjiga.autor_koautor, autori.prezime from izdanja join autori_knjiga on izdanja.isbn = autori_knjiga.ISBN_IZDANJE join autori on autori_knjiga.id_autor = autori.id'||wwv_flow.LF||
''||wwv_flow.LF||
'sel'||
'ect * from drzave'||wwv_flow.LF||
'select * from izdavaci'||wwv_flow.LF||
'select * from casopisi'||wwv_flow.LF||
'select * from radovi'||wwv_flow.LF||
''||wwv_flow.LF||
'select d.drzava, i.naziv, c.issn, c.kataloski_broj, c.urednik, r.naslov from drzave d join izdavaci i on d.id = i.id_drzava join casopisi c on i.id = c.id_izdavac join radovi r on c.issn = r.issn'||wwv_flow.LF||
''||wwv_flow.LF||
'select * from citaoci'||wwv_flow.LF||
''||wwv_flow.LF||
'with mali_citaoc as('||wwv_flow.LF||
'select BROJ_CLANSKE_KARTE, lower(ime), lower(prezime), pol, godina_rodjen'||
'ja, drzava, grad, adresa from citaoci'||wwv_flow.LF||
')'||wwv_flow.LF||
'select * from mali_citaoc where lower(mali_citaoc.ime) like ''m%'''||wwv_flow.LF||
''||wwv_flow.LF||
'select * from citaoci where lower(ime) like ''mol%'' or lower(prezime) like ''mol%'''||wwv_flow.LF||
'select * from knjiga'||wwv_flow.LF||
'select * from knjige where naslov like ''a%'''||wwv_flow.LF||
'select * from izdanja'||wwv_flow.LF||
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110038','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573580026845302
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573617425846069
 ,p_command => 
'select * from knjige where naziv like ''a%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573753961846959
 ,p_command => 
'select * from knjige where naziv like ''n%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573872266847196
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5573920851849914
 ,p_command => 
'select * from knjige where lower(naziv) like ''n%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110039','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574038902850551
 ,p_command => 
'select * from knjige'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110040','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574191103851762
 ,p_command => 
'select * from knjige where lower(naziv) like ''o%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110040','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574267444869731
 ,p_command => 
'select * from knjige where kataloski_broj like ''1%'' or lower(naziv) like ''o%'' or ostecenje like ''n%'' or mesto like ''a%'' or naziv_zanra like ''l%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110043','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574329085871210
 ,p_command => 
'select * from knjige where kataloski_broj like ''1%'' or lower(naziv) like ''o%'' or ostecenje like ''g%'' or mesto like ''a%'' or naziv_zanra like ''l%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110043','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574410010874283
 ,p_command => 
'select * from knjige where kataloski_broj like ''1%'' or lower(naziv) like ''o%'' or ostecenje like ''g%'' or mesto like ''d%'' or naziv_zanra like ''l%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110044','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574542057943837
 ,p_command => 
'select * from knjige where kataloski_broj like ''1%'' or lower(naziv) like ''o%'' or ostecenje like ''g%'' or mesto like ''a%'' or naziv_zanra like ''l%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110045','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574656430985759
 ,p_command => 
'select * from casopisi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110052','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574741118990215
 ,p_command => 
'select * from rad'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110053','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574886555991166
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110053','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5574926797002170
 ,p_command => 
'select * from radovi where lower(naslov) like ''d%'' or issn like ''%1'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110055','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5575004134004529
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110055','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5575139288005160
 ,p_command => 
'select * from radovi where lower(naslov) like ''m%'' or issn like ''%1'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110056','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5575276227006439
 ,p_command => 
'select * from radovi'
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110056','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
begin
  wwv_flow_api.create_sw_sql_cmds (
    p_id => 5575324541007804
 ,p_command => 
'select * from radovi where lower(naslov) like ''m%'' or issn like ''1%'''
    ,p_created_by => 'ADMIN'
    ,p_created_on => to_date('201709110056','YYYYMMDDHH24MI')
    ,p_parsed_schema => 'PROJEKATSEMA');
end;
/
----------------
--user access log
--
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709022059','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 5,
    p_custom_status_text => 'Invalid Login Credentials');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709022102','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709022102','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709022231','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709022304','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709031602','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709031951','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709032251','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709041540','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709042117','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709050227','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709062138','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709071653','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709071738','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709072249','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709072251','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709080313','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709081859','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709081909','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 4,
    p_custom_status_text => 'Invalid Login Credentials');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709081909','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709082057','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709082309','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709090246','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709091411','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709091724','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709091734','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709091958','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709092208','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709100019','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709101652','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709101816','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709102348','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709110218','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_050100',
    p_access_date => to_date('201709131354','YYYYMMDDHH24MI'),
    p_ip_address => '::1',
    p_remote_user => 'ANONYMOUS',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
prompt Check Compatibility...
begin
-- This date identifies the minimum version required to import this file.
wwv_flow_team_api.check_version(p_version_yyyy_mm_dd=>'2010.05.13');
end;
/
 
begin wwv_flow.g_import_in_progress := true; wwv_flow.g_user := USER; end; 
/
 
--
prompt ...news
--
begin
null;
end;
/
--
prompt ...links
--
begin
null;
end;
/
--
prompt ...bugs
--
begin
null;
end;
/
--
prompt ...events
--
begin
null;
end;
/
--
prompt ...feature types
--
begin
null;
end;
/
--
prompt ...features
--
begin
null;
end;
/
--
prompt ...feature map
--
begin
null;
end;
/
--
prompt ...tasks
--
begin
null;
end;
/
--
prompt ...feedback
--
begin
null;
end;
/
--
prompt ...task defaults
--
begin
null;
end;
/
 
prompt ...RESTful Services
 
begin
wwv_flow_api.remove_restful_service(
 p_id=>wwv_flow_api.id(5390932213295035)
,p_name=>'oracle.example.hr'
);
 
end;
/
prompt --application/restful_services/oracle_example_hr
begin
wwv_flow_api.create_restful_module(
 p_id=>wwv_flow_api.id(5390932213295035)
,p_name=>'oracle.example.hr'
,p_uri_prefix=>'hr/'
,p_parsing_schema=>'PROJEKATSEMA'
,p_items_per_page=>10
,p_status=>'PUBLISHED'
,p_row_version_number=>22
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5392955466295055)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'empinfo/'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5393046521295055)
,p_template_id=>wwv_flow_api.id(5392955466295055)
,p_source_type=>'QUERY'
,p_format=>'CSV'
,p_method=>'GET'
,p_require_https=>'NO'
,p_source=>'select * from emp'
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5391242987295049)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'employees/'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5391359726295049)
,p_template_id=>wwv_flow_api.id(5391242987295049)
,p_source_type=>'QUERY'
,p_format=>'DEFAULT'
,p_method=>'GET'
,p_items_per_page=>7
,p_require_https=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select empno "$uri", rn, empno, ename, job, hiredate, mgr, sal, comm, deptno',
'  from (',
'       select emp.*',
'            , row_number() over (order by empno) rn',
'         from emp',
'       ) tmp'))
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5391480501295049)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'employees/{id}'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5391569902295050)
,p_template_id=>wwv_flow_api.id(5391480501295049)
,p_source_type=>'QUERY_1_ROW'
,p_format=>'DEFAULT'
,p_method=>'GET'
,p_require_https=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from emp',
' where empno = :id'))
);
wwv_flow_api.create_restful_param(
 p_id=>wwv_flow_api.id(5391667794295052)
,p_handler_id=>wwv_flow_api.id(5391569902295050)
,p_name=>'ID'
,p_bind_variable_name=>'ID'
,p_source_type=>'HEADER'
,p_access_method=>'IN'
,p_param_type=>'STRING'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5391754988295053)
,p_template_id=>wwv_flow_api.id(5391480501295049)
,p_source_type=>'PLSQL'
,p_format=>'DEFAULT'
,p_method=>'PUT'
,p_require_https=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    update emp set ename = :ename, job = :job, hiredate = :hiredate, mgr = :mgr, sal = :sal, comm = :comm, deptno = :deptno',
'    where empno = :id;',
'    :status := 200;',
'    :location := :id;',
'exception',
'    when others then',
'        :status := 400;',
'end;'))
);
wwv_flow_api.create_restful_param(
 p_id=>wwv_flow_api.id(5391813540295053)
,p_handler_id=>wwv_flow_api.id(5391754988295053)
,p_name=>'ID'
,p_bind_variable_name=>'ID'
,p_source_type=>'HEADER'
,p_access_method=>'IN'
,p_param_type=>'STRING'
);
wwv_flow_api.create_restful_param(
 p_id=>wwv_flow_api.id(5392032320295054)
,p_handler_id=>wwv_flow_api.id(5391754988295053)
,p_name=>'X-APEX-FORWARD'
,p_bind_variable_name=>'location'
,p_source_type=>'HEADER'
,p_access_method=>'OUT'
,p_param_type=>'STRING'
);
wwv_flow_api.create_restful_param(
 p_id=>wwv_flow_api.id(5391921398295053)
,p_handler_id=>wwv_flow_api.id(5391754988295053)
,p_name=>'X-APEX-STATUS-CODE'
,p_bind_variable_name=>'status'
,p_source_type=>'HEADER'
,p_access_method=>'OUT'
,p_param_type=>'INT'
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5392141981295054)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'employeesfeed/'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5392207210295054)
,p_template_id=>wwv_flow_api.id(5392141981295054)
,p_source_type=>'FEED'
,p_format=>'DEFAULT'
,p_method=>'GET'
,p_items_per_page=>25
,p_require_https=>'NO'
,p_source=>'select empno, ename from emp order by deptno, ename'
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5392369731295054)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'employeesfeed/{id}'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5392426971295054)
,p_template_id=>wwv_flow_api.id(5392369731295054)
,p_source_type=>'QUERY'
,p_format=>'CSV'
,p_method=>'GET'
,p_require_https=>'NO'
,p_source=>'select * from emp where empno = :id'
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5392526075295054)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'empsec/{empname}'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5392625479295054)
,p_template_id=>wwv_flow_api.id(5392526075295054)
,p_source_type=>'QUERY'
,p_format=>'DEFAULT'
,p_method=>'GET'
,p_require_https=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select empno, ename, deptno, job from emp ',
'	where ((select job from emp where ename = :empname) IN (''PRESIDENT'', ''MANAGER'')) ',
'    OR  ',
'    (deptno = (select deptno from emp where ename = :empname)) ',
'order by deptno, ename',
''))
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5392708585295055)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'empsecformat/{empname}'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5392809214295055)
,p_template_id=>wwv_flow_api.id(5392708585295055)
,p_source_type=>'PLSQL'
,p_format=>'DEFAULT'
,p_method=>'GET'
,p_require_https=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  prevdeptno     number;',
'  total_rows     number;',
'  deptloc        varchar2(20);',
'  deptname       varchar2(20);',
'  ',
'  CURSOR         getemps is select * from emp ',
'                             start with ename = :empname',
'                           connect by prior empno = mgr',
'                             order siblings by deptno, ename;',
'BEGIN',
'  sys.htp.htmlopen;',
'  sys.htp.headopen;',
'  sys.htp.title(''Hierarchical Department Report for Employee ''||apex_escape.html(:empname));',
'  sys.htp.headclose;',
'  sys.htp.bodyopen;',
' ',
'  for l_employee in getemps ',
'  loop',
'      if l_employee.deptno != prevdeptno or prevdeptno is null then',
'          select dname, loc ',
'            into deptname, deptloc ',
'            from dept ',
'           where deptno = l_employee.deptno;',
'           ',
'          if prevdeptno is not null then',
'              sys.htp.print(''</ul>'');',
'          end if;',
'',
'          sys.htp.print(''Department '' || apex_escape.html(deptname) || '' located in '' || apex_escape.html(deptloc) || ''<p/>'');',
'          sys.htp.print(''<ul>'');',
'      end if;',
'',
'      sys.htp.print(''<li>'' || apex_escape.html(l_employee.ename) || '', ''  || apex_escape.html(l_employee.empno) || '', '' || ',
'                        apex_escape.html(l_employee.job) || '', '' || apex_escape.html(l_employee.sal) || ''</li>'');',
'',
'      prevdeptno := l_employee.deptno;',
'      total_rows := getemps%ROWCOUNT;',
'      ',
'  end loop;',
'',
'  if total_rows > 0 then',
'      sys.htp.print(''</ul>'');',
'  end if;',
'',
'  sys.htp.bodyclose;',
'  sys.htp.htmlclose;',
'END;'))
);
wwv_flow_api.create_restful_template(
 p_id=>wwv_flow_api.id(5391058623295043)
,p_module_id=>wwv_flow_api.id(5390932213295035)
,p_uri_template=>'version/'
,p_priority=>0
,p_etag_type=>'HASH'
);
wwv_flow_api.create_restful_handler(
 p_id=>wwv_flow_api.id(5391159273295044)
,p_template_id=>wwv_flow_api.id(5391058623295043)
,p_source_type=>'PLSQL'
,p_format=>'DEFAULT'
,p_method=>'GET'
,p_require_https=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    sys.htp.p( ''{ "version": "5.1" }'' );',
'end;'))
);
end;
/
-- SET SCHEMA
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_schema   := 'PROJEKATSEMA';
   wwv_flow_hint.check_schema_privs;
 
end;
/

 
--------------------------------------------------------------------
prompt  SCHEMA PROJEKATSEMA - User Interface Defaults, Table Defaults
--
-- Import using sqlplus as the Oracle user: APEX_050100
-- Exported 14:08 Wednesday September 13, 2017 by: ADMIN
--
 
--------------------------------------------------------------------
prompt User Interface Defaults, Attribute Dictionary
--
-- Exported 14:08 Wednesday September 13, 2017 by: ADMIN
--
-- SHOW EXPORTING WORKSPACE
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_exp_workspace := 'WORKSPACEPROJEKAT';
 
end;
/

begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
